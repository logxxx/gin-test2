package largestring

import (
	"github.com/patrickmn/go-cache"
	"io/ioutil"
	"time"
)

var (
	localCache = cache.New(1*time.Hour, 2*time.Hour)
)

func GetLargeString() ([]byte, error) {
	iContent, ok := localCache.Get("large_string_cache")
	if ok {
		return iContent.([]byte), nil
	}

	contentAll, err := ioutil.ReadFile("./source/herry.txt")
	if err != nil {
		return nil, err
	}

	localCache.SetDefault("large_string_cache", contentAll)

	return contentAll, nil

}
