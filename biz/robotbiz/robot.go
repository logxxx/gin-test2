package robotbiz

import (
	"encoding/xml"
	"fmt"
	"logxxx.com/test/v2/biz/fund"
	"logxxx.com/test/v2/biz/yuqian"
	"logxxx.com/test/v2/utils/log"
	"time"
)

type CDATAStr struct {
	Text string `xml:",cdata"`
}

type CDATANum struct {
	Num int64 `xml:",cdata"`
}

type ReplyMsg2 struct {
	XMLName      xml.Name `xml:"xml"`
	ToUserName   CDATAStr `xml:"ToUserName"`
	FromUserName CDATAStr `xml:"FromUserName"`
	CreateTime   CDATANum `xml:"CreateTime"`
	MsgType      CDATAStr `xml:"MsgType"`
	Content      CDATAStr `xml:"Content"`
	MsgId        CDATANum `xml:"MsgId"`
}

type ReplyMsg struct {
	ToUserName   string `xml:"ToUserName"`
	FromUserName string `xml:"FromUserName"`
	CreateTime   int64  `xml:"CreateTime"`
	MsgType      string `xml:"MsgType"`
	Content      string `xml:"Content"`
	MsgId        int64  `xml:"MsgId"`
}

type ReplyReq ReplyMsg

type ReplyResp ReplyMsg

func ParseReplyRequest(rawReply []byte) (*ReplyReq, error) {
	reply := &ReplyReq{}
	err := xml.Unmarshal(rawReply, reply)
	if err != nil {
		return nil, err
	}
	return reply, nil
}

func GeneReplyMsg(req *ReplyReq) (*ReplyMsg2, error) {
	var err error
	content := ""

	if len(req.Content) >= 2 && req.Content[:2] == "jj" {
		if req.Content == "jj" {
			content = fmt.Sprintf("请输入jj+基金代号,如:\njj011453 华泰柏瑞医疗健康C\njj161616 通融医疗保健行业混合A/B")
		} else {
			content, err = fund.GetFundBaseInfo(req.Content)
			if err != nil {
				log.Errorf("GeneReplyMsg GetFundBaseInfo err:%v", err)
				content = fmt.Sprintf("没有找到这只鸡!请检查鸡号。")
			}
		}
	} else {
		content = yuqian.GetRandomWordFromAll()
	}

	reply := &ReplyMsg2{
		ToUserName:   CDATAStr{req.FromUserName},
		FromUserName: CDATAStr{req.ToUserName},
		CreateTime:   CDATANum{time.Now().Unix()},
		MsgType:      CDATAStr{"text"},
		Content:      CDATAStr{content},
	}
	return reply, nil
}
