package story

import (
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"logxxx.com/test/v2/utils/log"
	"os"
)

var (
	cfg = &StoryConfig{}
)

type StoryConfig struct {
	Scenes []*SceneCfg `yaml:"Scenes"`
}

type SceneCfg struct {
	Id         int          `yaml:"Id"`
	Title      string       `yaml:"Title"`
	Content    string       `yaml:"Content"`
	OptionCfgs []*OptionCfg `yaml:"OptionCfgs"`
	TriggerCfg *Trigger     `yaml:"TriggerCfg"`
}

type Trigger struct {
	AddStr int `yaml:"AddStr"` //加力量
	AddInt int `yaml:"AddInt"` //加智力
	AddAgi int `yaml:"AddAgi"` //加敏捷
	AddMon int `yaml:"AddMon"` //加金钱
}

type OptionCfg struct {
	Id      int    `yaml:"Id"`
	Title   string `yaml:"Title"`
	Content string `yaml:"Content"`
	SceneId int    `yaml:"SceneId"`
}

func InitConfig() error {
	file, err := os.Open("conf/story.yml")
	if err != nil {
		log.Errorf("InitConfig Open err:%v", err)
		return err
	}
	rawCfg, err := ioutil.ReadAll(file)
	if err != nil {
		log.Errorf("InitConfig ReadAll err:%v", err)
		return err
	}

	err = yaml.Unmarshal(rawCfg, cfg)
	if err != nil {
		log.Errorf("InitConfig Unmarshal err:%v", err)
		return err
	}

	return nil
}

func GetConfig() *StoryConfig {
	return cfg
}
