package story

type Scene struct {
	Id        int
	Title     string
	Content   string
	OptionIds []int
}

type Option struct {
	Id      int
	Title   string
	Content string
	SceneId int
}
