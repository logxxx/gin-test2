package fund

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"logxxx.com/test/v2/utils"
	"logxxx.com/test/v2/utils/log"
	"net/http"
)

type FundInfo struct {
	FundCode string `json:"fundcode"` //基金代码
	Name     string `json:"name"`     //基金名称
	Jzrq     string `json:"jzrq"`     //基金开始日期
	Dwjz     string `json:"dwjz"`     //净值
	Gsz      string `json:"gsz"`      //估算净值
	Gszzl    string `json:"gszzl"`    //估算涨跌幅
	Gztime   string `json:"gztime"`   //时间
}

func GetFundBaseInfo(content string) (string, error) {
	jijinCode := content[2:]
	getJijinApi := fmt.Sprintf("http://fundgz.1234567.com.cn/js/%v.js", jijinCode)

	resp, err := http.DefaultClient.Get(getJijinApi)
	if err != nil {
		return "", err
	}
	defer resp.Body.Close()

	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	fundInfoJson := utils.GetBetweenStr(string(respBody), "(", ")")
	log.Infof("fundInfoJson:%v", fundInfoJson)

	fundInfo := &FundInfo{}
	err = json.Unmarshal([]byte(fundInfoJson), fundInfo)
	if err != nil {
		return "", err
	}

	showFundInfo := fmt.Sprintf("基金代码:%v\n基金名称:%v\n开始日期:%v\n净值:%v\n估算净值:%v\n估算涨跌幅:%v\n时间:%v",
		fundInfo.FundCode, fundInfo.Name, fundInfo.Jzrq, fundInfo.Dwjz, fundInfo.Gsz, fundInfo.Gszzl, fundInfo.Gztime)

	log.Infof("showFundInfo:%v", showFundInfo)

	return showFundInfo, nil
}
