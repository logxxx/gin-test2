package yuqian

import (
	"math/rand"
	"time"
)

var (
	all []string
)

func init() {
	allArr := [][]string{
		GetAllMomo(), GetAllSuprise(), GetAllAgree(), GetAllDoubt(), GetAllDoubt(),
	}
	for _, v := range allArr {
		all = append(all, v...)
	}
}

func GetRandomWordFromAll() string {
	rand.Seed(time.Now().UnixNano())
	return all[rand.Intn(len(all))]
}

func GetAllMomo() []string {
	return []string{"哈哈，你说呢？",
		"怎么说呢，还可以吧。",
		"不好说。",
		"你也知道。",
		"也许忘记最好。",
		"不要轻易付出真心!",
		"你的初心呢？",
		"这得看你怎么想了。",
		"你这么说，也没毛病。",
		"我觉得你已经有答案了。",
		"那你觉得呢？",
		"你这么想也对。",
	}
}

func GetAllAgree() []string {
	return []string{
		"好。",
		"对!",
		"当然啦。",
		"就是嘛。",
		"没错!",
		"可不是么!",
		"是。",
		"谁说不是呢!",
	}
}

func GetAllDoubt() []string {
	return []string{
		"啊?",
		"干嘛呀?",
		"怎么啦?",
		"像话吗?",
		"没听说过!",
		"不怎么样!",
		"就这?",
		"恩?",
		"你说谁呢?",
		"多新鲜呢",
		"你死不死",
		"我呀?",
	}
}

func GetAllSuprise() []string {
	return []string{
		"嘿!",
		"害!",
		"嚯!",
		"哎呀",
		"哟!",
		"哎呦喂!",
		"去你的吧!",
		"好家伙!",
	}
}

func GetRandomAnswer() string {
	nowUnix := int(time.Now().UnixNano())
	result := nowUnix % 3
	switch result {
	case 0:
		return GetAllAgree()[nowUnix%len(GetAllAgree())]
	case 1:
		return GetAllDoubt()[nowUnix%len(GetAllDoubt())]
	case 2:
		return GetAllSuprise()[nowUnix%len(GetAllSuprise())]
	default:
		return GetAllAgree()[nowUnix%len(GetAllAgree())]
	}
}
