package rangzidanfei

import (
	"bufio"
	"io"
	"os"
	"strings"
)

var (
	mgr *FeiMgr
)

type FeiMgr struct {
	Idx   int
	Words []string
}

func (mgr *FeiMgr) GetWord() string {
	resp := mgr.Words[mgr.Idx]
	mgr.Idx += 1
	if mgr.Idx >= len(mgr.Words) {
		mgr.Idx = 0
	}
	return resp
}

func Parse() []string {
	resp := make([]string, 0)

	f, err := os.Open("src/source/raw.txt")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	rd := bufio.NewReader(f)
	for {
		line, err := rd.ReadString('\n') //以'\n'为结束符读入一行

		if err != nil || io.EOF == err {
			break
		}
		parts := strings.Split(line, "：")
		if len(parts) != 2 {
			continue
		}
		resp = append(resp, parts[1])
	}
	return resp
}

func ResetFeiMgr(idx int) {
	m := &FeiMgr{
		Words: Parse(),
	}
	m.Idx = idx % len(m.Words)
	mgr = m
}

func GetFeiMgr() *FeiMgr {
	if mgr == nil {
		ResetFeiMgr(0)
	}
	return mgr
}
