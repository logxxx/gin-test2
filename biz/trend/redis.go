package trend

import (
	"bytes"
	"encoding/json"
	"gopkg.in/redis.v3"
	"io"
	"time"
)

var (
	_redis *Redis
)

type Redis struct {
	rdb *redis.Client
}

func GetRedisClient() *Redis {
	if _redis != nil {
		return _redis
	}
	_redis = &Redis{
		rdb: redis.NewClient(&redis.Options{
			Addr:     "49.232.219.233:6379",
			Password: "he1234",
		}),
	}

	return _redis
}

func (r *Redis) GetRdb() *redis.Client {
	return r.rdb
}

func (r *Redis) IncrBy(key string, value int64) error {
	r.rdb.Expire(key, 30*time.Minute)
	return r.rdb.IncrBy(key, value).Err()
}

func (r *Redis) Get(key string) (int64, error) {
	return r.rdb.Get(key).Int64()
}

type TrendDataReq struct {
	Key   string `json:"key"`
	Field string `json:"field"`
	Value string `json:"value"`
}

func getReqData(key, field, value string) io.Reader {
	req := &TrendDataReq{
		Key:   key,
		Field: field,
		Value: value,
	}
	reqJson, _ := json.Marshal(req)

	return bytes.NewBuffer(reqJson)
}
