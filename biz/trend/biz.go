package trend

import (
	"log"
	"strconv"
	"time"
)

var (
	lastCache = make(map[string]int64, 0)
)

func StartCollect() {
	t := time.Tick(1 * time.Second)
	for {
		select {
		case <-t:
			go GetDataAndReportToProm(time.Now())
			t = time.Tick(60 * time.Second)
		}
	}
}

func GetDataAndReportToProm(now time.Time) {

	log.Printf("GetDataAndReportToProm start")

	//keys := GetRedisClient().GetRdb().SMembers("trendkeys").Val() //格式不对

	keys := GetRedisClient().GetRdb().Keys("*_*").Val()

	metrics := make(map[string]string, 0)
	for _, key := range keys {
		val := getValue(key, now)
		if needSend(key, val) {
			metrics[key] = strconv.FormatInt(val, 10)
		}
	}

	SendAsGaugeBatch(metrics)
}

func getValue(key string, now time.Time) int64 {
	t := now.Add(-10 * time.Minute)
	t = t.Add(-time.Duration(t.Unix()%30) * time.Second)
	field1 := t.Format("20060102_150405")
	field2 := t.Add(-30 * time.Second).Format("20060102_150405")
	val1, _ := GetRedisClient().GetRdb().HGet(key, field1).Int64()
	val2, _ := GetRedisClient().GetRdb().HGet(key, field2).Int64()
	//log.Printf("key:%v\nf1:%v v1:%v f2:%v v2:%v", key, field1, val1, field2, val2)
	return val1 + val2
}

func needSend(key string, value int64) bool {

	lastValue, ok := lastCache[key]
	if !ok {
		lastCache[key] = value
		return true
	}
	if value == 0 && lastValue == 0 { //连续为0
		return false
	}

	lastCache[key] = value
	return true
}
