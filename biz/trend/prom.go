package trend

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
)

func parseLabel(key string) (string, string, string) {
	//cntby_task_xlppcdrive_common_container_drive_log_NONE
	elems := strings.Split(key, "_")
	if len(elems) < 3 {
		return "", "", ""
	}
	//倒数第二个是label_key, 倒数第一个是label_value
	labelKey := elems[len(elems)-2]
	labelValue := elems[len(elems)-1]

	return strings.Join(elems[:len(elems)-2], "_"), labelKey, labelValue
}

func SendAsGaugeBatch(dataMap map[string]string) error {
	url := "http://49.232.219.233:9091/metrics/job/trend_0914"

	type Metric struct {
		Key    string
		Labels []string
	}

	metrics := make(map[string]*Metric, 0)

	for k, v := range dataMap {
		newKey, labelK, labelV := parseLabel(k)
		if newKey == "" {
			continue
		}
		if metric, ok := metrics[newKey]; ok {
			metric.Labels = append(metric.Labels, fmt.Sprintf(`%v{%v="%v"} %v`, newKey, labelK, labelV, v))
		} else {
			newMetric := &Metric{
				Key:    newKey,
				Labels: []string{fmt.Sprintf(`%v{%v="%v"} %v`, newKey, labelK, labelV, v)},
			}
			metrics[newKey] = newMetric
		}
	}

	reqData := ""
	for _, metric := range metrics {
		elem := fmt.Sprintf("\n# TYPE %v gauge", metric.Key)
		for _, label := range metric.Labels {
			elem += fmt.Sprintf("\n%v\n", label)
		}
		reqData += elem
	}
	err := sendCore(url, reqData)
	if err != nil {
		return err
	}
	return nil
}

func SendAsGauge(key string, value float64, labels map[string]string) error {
	url := "http://49.232.219.233:9092/metrics/job/test_trend_mocktime"

	labelsStr := ""
	if len(labels) > 0 {
		labelsPair := []string{}
		for k, v := range labels {
			labelsPair = append(labelsPair, fmt.Sprintf(`%v="%v"`, k, v))
		}
		labelsStr = fmt.Sprintf("{%v}", strings.Join(labelsPair, ","))
	}

	reqData := fmt.Sprintf(`
# TYPE %v gauge
%v %v%v %v
`, key, key, labelsStr, value, time.Now().Add(-7*time.Hour*24).UnixNano())
	err := sendCore(url, reqData)
	if err != nil {
		return err
	}
	return nil
}

func sendCore(url, data string) error {

	//log.Printf("Send req:%+v", data)

	reqBuf := bytes.NewBufferString(data)
	req, err := http.NewRequest("POST", url, reqBuf)
	if err != nil {
		return err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	respData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	log.Printf("Send resp:%+v", string(respData))

	return nil
}
