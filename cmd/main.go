package main

import (
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"log"
	"logxxx.com/test/v2/biz/trend"
	"logxxx.com/test/v2/controller"
	"logxxx.com/test/v2/utils/webutil"
	"net/http"
	"os"
)

func runServer() {
	r := gin.Default()
	r.Use(webutil.PrintURL)
	r.Use(Cors())
	r.GET("/ping", func(c *gin.Context) {
		webutil.Return(c, gin.H{
			"message": "pong",
		})
	})
	r.GET("robot/get/reply", controller.GetReply)
	r.POST("robot/get/reply", controller.PostReply)
	r.POST("test/json1", controller.TestJson1)
	r.POST("/myprom/report_trend_data", controller.ReportTrendData)
	r.POST("/upload", controller.UploadFile)
	r.POST("/download", controller.DownloadFile)
	r.Any("/testspeed/:page", func(c *gin.Context) {
		//time.Sleep(time.Duration(randutil.RandIntn(200)) * time.Millisecond)
		page := c.Param("page")
		wd, _ := os.Getwd()
		log.Printf("page:%v wd:%v", page, wd)
		c.String(200, "")
		//c.File(fmt.Sprintf("testspeed/%v", page))
	})
	r.GET("/apps/install_pkg/:appid", func(c *gin.Context) {
		appID := c.Param("appid")
		log.Printf("get APPID:%v", appID)

		wd, _ := os.Getwd()
		log.Printf("wd:%v", wd)

		fileName := appID + ".tar.gz"
		appBytes, err := ioutil.ReadFile("./apps/" + fileName)
		if err != nil {
			log.Printf("get app ReadFile err:%v appID:%v", err, appID)
			c.String(500, err.Error())
			return
		}

		fileDesc := "attachment;filename=\"" + fileName + "\""
		c.Header("Content-Type", "application/x-tar")
		c.Header("Content-Disposition", fileDesc)
		c.Data(http.StatusOK, "application/x-tar", appBytes)
	})

	controller.RegisterTestApis(r)

	r.Run(":8080") // 在 0.0.0.0:8080 上监听并服务

}

func main() {

	go func() {
		trend.StartCollect()
	}()

	runServer()
}

func Cors() gin.HandlerFunc {
	return func(c *gin.Context) {
		method := c.Request.Method
		c.Header("Access-Control-Allow-Origin", "*") // 可将将 * 替换为指定的域名
		c.Header("Access-Control-Allow-Methods", "*")
		c.Header("Access-Control-Allow-Headers", "*")
		c.Header("Access-Control-Expose-Headers", "*")
		c.Header("Access-Control-Allow-Credentials", "true")

		if method == "OPTIONS" {
			c.AbortWithStatus(http.StatusNoContent)
		}
		c.Next()
	}
}
