#!/bin/bash
echo "开始自动执行..."
git pull
docker stop test_robot1
docker rm test_robot1
docker build -t test_robot1 .
docker run -d -p 80:8080 --name test_robot1 test_robot1
docker logs test_robot1 -f --tail=20
