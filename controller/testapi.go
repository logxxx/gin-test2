package controller

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"logxxx.com/test/v2/biz/largestring"
	"logxxx.com/test/v2/utils/jsonutil"
	"logxxx.com/test/v2/utils/randutil"
	"logxxx.com/test/v2/utils/webutil"
	"strconv"
	"time"
)

func RegisterTestApis(r *gin.Engine) {

	r.POST("/test/return_json", func(c *gin.Context) {
		rawData, err := c.GetRawData()
		if err != nil {
			webutil.ReturnErr(c, err)
			return
		}

		j, err := jsonutil.NewJSON(rawData)
		if err != nil {
			webutil.ReturnErr(c, err)
			return
		}

		webutil.Return(c, j.Interface())

	})

	r.POST("/test/parse_string_to_json", func(c *gin.Context) {
		req := &struct {
			Input string `json:"input"`
		}{}
		webutil.ParseReq(c, req)

		j, err := jsonutil.NewJSON([]byte(req.Input))
		if err != nil {
			webutil.ReturnErr(c, err)
			return
		}

		webutil.Return(c, j.Interface())

	})

	r.GET("/test/return_complex_struct", func(c *gin.Context) {
		output := `{"bbs":{"hasMore":true,"list":[{"id":1062765,"circle_id":12,"title":"83版的TVB《射雕英雄传》不是最早的电视剧版，76版才是","abstract":"83版的TVB《射雕英雄传》不是最早的电视剧版，76版米雪主演的才是。76版《射","code_word":"","pic":"http://pc.wangpan.xycdn.n0808.com/9eb6f14d657da3270ec7bc0c6bea7cdb","create_time":1563767346705,"author":{"uid":"41422305","nick_name":"墨韵","portrait_url":"https://xfile2.a.88cdn.com/file/k/avatar/default/300x300","newno":"613343262"}},{"id":1077277,"circle_id":3,"title":"金庸如何评价：米雪版《射雕英雄传》（1976）","abstract":"《射雕英雄传》里的黄蓉一直是金庸笔下最丰满的角色之一，也深受大家喜爱。“俏黄蓉”","code_word":"","pic":"","create_time":1565447333702,"author":{"uid":"699833870","nick_name":"黯","portrait_url":"https://xfile2.a.88cdn.com/file/k/avatar/default/300x300","newno":"1402287929"}},{"id":1578909,"circle_id":44,"title":"射雕英雄传的CP中，你最喜欢哪一对？","abstract":"《射雕英雄传》总共被改编了8次 ，中间的这几对CP，你最喜欢哪一对？  1976年香港佳视版电视剧白彪 饰 郭靖米雪 饰 黄蓉1983年 香港TVB版电视剧黄日华 饰 郭靖翁美玲 饰 黄蓉1988年台湾中视版电视剧黄文豪 饰 郭靖陈玉莲 饰 黄蓉1994年香港TVB版电视剧张智霖 饰 郭靖朱茵 饰 黄蓉2003年内地版","code_word":"","pic":"http://pc.wangpan.xycdn.n0808.com/fd0371382517989be6fd95bfe2b4eb8f","create_time":1570789109771,"author":{"uid":"159547206","nick_name":"核成的咸大","portrait_url":"https://xfile2.a.88cdn.com/file/k/159547206/avatar/AV3rqA.jpg/300x300","newno":"578064293"}},{"id":1110100,"circle_id":50,"title":"《射雕英雄传》有哪些你想要吐槽的点？","abstract":"听说看片后不吐槽一下的话，对不起自己的不开心，快来和小伙伴们一起畅快吐槽吧","code_word":"","pic":"","create_time":1568202632660,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1101864,"circle_id":50,"title":"看完作品《射雕英雄传》后，你有什么想法？","abstract":"每看一部作品，都体验了一次小人生，快来分享内心的想法吧","code_word":"","pic":"","create_time":1568198692237,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1125797,"circle_id":50,"title":"《射雕英雄传》有哪些你想要吐槽的点？","abstract":"听说看片后不吐槽一下的话，对不起自己的不开心，快来和小伙伴们一起畅快吐槽吧","code_word":"","pic":"","create_time":1568208504168,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1126128,"circle_id":50,"title":"看完作品《射雕英雄传》后，你有什么想法？","abstract":"每看一部作品，都体验了一次小人生，快来分享内心的想法吧","code_word":"","pic":"","create_time":1568208683752,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1101863,"circle_id":50,"title":"《射雕英雄传》有哪些你想要吐槽的点？","abstract":"听说看片后不吐槽一下的话，对不起自己的不开心，快来和小伙伴们一起畅快吐槽吧","code_word":"","pic":"","create_time":1568198692216,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1126127,"circle_id":50,"title":"《射雕英雄传》有哪些你想要吐槽的点？","abstract":"听说看片后不吐槽一下的话，对不起自己的不开心，快来和小伙伴们一起畅快吐槽吧","code_word":"","pic":"","create_time":1568208683734,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1172416,"circle_id":50,"title":"看完作品《射雕英雄传》后，你有什么想法？","abstract":"每看一部作品，都体验了一次小人生，快来分享内心的想法吧","code_word":"","pic":"","create_time":1568226915922,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1318105,"circle_id":50,"title":"看完作品《射雕英雄传》后，你有什么想法？","abstract":"每看一部作品，都体验了一次小人生，快来分享内心的想法吧","code_word":"","pic":"","create_time":1568331295768,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1403641,"circle_id":50,"title":"《射雕英雄传》有哪些你想要吐槽的点？","abstract":"听说看片后不吐槽一下的话，对不起自己的不开心，快来和小伙伴们一起畅快吐槽吧","code_word":"","pic":"","create_time":1568445309133,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1403642,"circle_id":50,"title":"看完作品《射雕英雄传》后，你有什么想法？","abstract":"每看一部作品，都体验了一次小人生，快来分享内心的想法吧","code_word":"","pic":"","create_time":1568445309143,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1126130,"circle_id":50,"title":"《射雕英雄传》里有让你难以忘怀的情节么？","abstract":"快来分享让你难以忘怀的情节吧，很多人都会有所共鸣的","code_word":"","pic":"","create_time":1568208683790,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1216690,"circle_id":50,"title":"《射雕英雄传》有哪些你想要吐槽的点？","abstract":"听说看片后不吐槽一下的话，对不起自己的不开心，快来和小伙伴们一起畅快吐槽吧","code_word":"","pic":"","create_time":1568255366180,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1125798,"circle_id":50,"title":"看完作品《射雕英雄传》后，你有什么想法？","abstract":"每看一部作品，都体验了一次小人生，快来分享内心的想法吧","code_word":"","pic":"","create_time":1568208504180,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1172415,"circle_id":50,"title":"《射雕英雄传》有哪些你想要吐槽的点？","abstract":"听说看片后不吐槽一下的话，对不起自己的不开心，快来和小伙伴们一起畅快吐槽吧","code_word":"","pic":"","create_time":1568226915901,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1216691,"circle_id":50,"title":"看完作品《射雕英雄传》后，你有什么想法？","abstract":"每看一部作品，都体验了一次小人生，快来分享内心的想法吧","code_word":"","pic":"","create_time":1568255366206,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1266844,"circle_id":50,"title":"《射雕英雄传之东成西就》有哪些你想要吐槽的点？","abstract":"听说看片后不吐槽一下的话，对不起自己的不开心，快来和小伙伴们一起畅快吐槽吧","code_word":"","pic":"","create_time":1568293867246,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}},{"id":1231536,"circle_id":50,"title":"看完作品《射雕英雄传续集》后，你有什么想法？","abstract":"每看一部作品，都体验了一次小人生，快来分享内心的想法吧","code_word":"","pic":"","create_time":1568266525303,"author":{"uid":"609637799","nick_name":"迅雷院线","portrait_url":"https://xfile2.a.88cdn.com/file/k/FooId0hyeZP1b-ERoPl31GcHLhsE/300x300","newno":"548914566"}}],"total":95},"cinecism":{"count":1,"list":[{"id":112508,"body_url":"https://api-shoulei-ssl.xunlei.com/cinecism/api/v4/content/render?cid=112508","cover_url":"http://pc.wangpan.xycdn.n0808.com/093d95690bd68fd395dccc9c6cd4907e","create_time":1537526792,"media_id":"60709","share_count":0,"show_count":2416,"title":"90后的你，还记得“魔神英雄传”这部动漫吗？","summary":"曾经在国内的电视上播出过，叫神龙斗士。不知道还有没有印象？趁着这次机会在这里也科普一波。首先是关于大量的“没看过”的问题，神龙斗士的1+2是88年和90年的动画，大陆在93年引进。然后超魔神是在97年","uid":692844340,"user_info":{"biography":"","birthday":"19900504","description":"","kind":"per","kinds":["review_expert"],"live_extra":{},"login_time":"2021-07-01 10:33:52","newno":"1291606351","nick_name":"Victor","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/Fmr7F2urtt3kl9MYE7_NcDfs97Sn/300x300","province":"云南省","pub_extra":{"uid":692844340,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-11-21 19:11:54","sex":"female","uid":"692844340","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"692844340","vasID":2,"vasType":3},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"692844340","vasID":14,"vasType":0}]}}],"total":316},"hotdrama":{"hasMore":false,"videoList":null},"message":"","pub":{"count":1,"list":[{"uid":"744331063","title":"漫威DC英雄传","icon_url":"https://xfile2.a.88cdn.com/file/k/744331063/avatar/1545430161.jpg/300x300","description":" ","v_status":0,"kind":"meipai","fans_count":0,"follow_count":0,"live_extra":null,"is_follow":false,"visit_count":4,"tags":[""]}],"total":66},"result":0,"site":{"count":30,"list":[{"id":1058815,"url":"https://m.80s.tw/movie/19624","title":"射雕英雄传2017版","poster":"http://sl.wangpan.7niu.n0808.com/1b4f32e97179493f8163ace8bd4d9da45e21adfe","user_info":{"biography":"追星，追到形影不离","birthday":"20120819","description":"追星，追到形影不离","kind":"per","kinds":["per"],"live_extra":{},"login_time":"","newno":"1246856263","nick_name":"HiHi佳爷","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/FpdtzZkORqURw49_L0rFNp2Jx53U/300x300","province":"湖南省","pub_extra":{"uid":672092608,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-08-02 15:20:23","sex":"female","uid":"672092608","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"672092608","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"672092608","vasID":14,"vasType":0}]}},{"id":939478,"url":"http://www.piaohua.com/webPlay/play-id-31684-collection-30.html","title":"射雕英雄传 - 在线播放","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2018-01-11 03:55:59","newno":"559382784","nick_name":"失灬态","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/Ftwa8JiD8hw-rTOySLMplKlr_Ukm/300x300","province":"河南省","pub_extra":{"uid":617439815,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2016-12-29 22:51:18","sex":"unknown","uid":"617439815","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"617439815","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"617439815","vasID":14,"vasType":0}]}},{"id":1047762,"url":"https://m.80s.tw/movie/3029","title":"08版射雕英雄传","poster":"http://sl.wangpan.7niu.n0808.com/c90a3197aa0d3f39dd535c57049ab4889b76bd5d","user_info":{"biography":"哥是传奇","birthday":"19930420","description":"哥是传奇","kind":"per","kinds":["per"],"live_extra":{},"login_time":"","newno":"1246870220","nick_name":"手机用户2386308925","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/FmxInI4NqpdzmOppPmjDvhoTedw1/300x300","province":"内蒙古自治区","pub_extra":{"uid":672105297,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-08-02 15:56:02","sex":"male","uid":"672105297","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"672105297","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"672105297","vasID":14,"vasType":0}]}},{"id":892928,"url":"https://m.80s.tw/movie/19624","title":"射雕英雄传2017版 (2017) 8.1分","poster":"http://sl.wangpan.7niu.n0808.com/1b4f32e97179493f8163ace8bd4d9da45e21adfe","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"","newno":"","nick_name":"用户不存在","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"http://7xnvqo.com1.z0.glb.clouddn.com/","province":"","pub_extra":{"uid":3011456,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"","sex":"","uid":"3011456","vip_extra":[{"VIPLevel":7,"isVIP":0,"isYear":1,"uid":"3011456","vasID":2,"vasType":3},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"3011456","vasID":14,"vasType":0}]}},{"id":941032,"url":"http://www.piaohua.com/webPlay/play-id-31994-collection-4.html","title":"射雕英雄传2017(粤语) - 在线播放","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2019-05-09 01:45:25","newno":"1274503441","nick_name":"南城旧梦","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/Fk6EKqkkLKvC0FGS8LsBEB1mIvzt/300x300","province":"甘肃省","pub_extra":{"uid":687168895,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-10-22 23:17:28","sex":"male","uid":"687168895","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"687168895","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"687168895","vasID":14,"vasType":0}]}},{"id":515470,"url":"http://www.dygang.net/dsj/20110107/c5ca86291b8d5efb7261fbc4a10f0ff6.htm","title":"射雕英雄传83版[全集]_免费下载_国剧_电影港","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2020-03-10 19:16:43","newno":"1266963716","nick_name":"小太阳","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/Fs_v9rsoeOLzws0UeS0_dmjMYi9Y/300x300","province":"湖南省","pub_extra":{"uid":681315650,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-09-21 07:48:53","sex":"male","uid":"681315650","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"681315650","vasID":2,"vasType":3},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"681315650","vasID":14,"vasType":0}]}},{"id":1304816,"url":"http://m.xunleicang.com/vod-read-id-28650.html","title":"射雕英雄传[张智霖版]_迅雷下载_高清电影_迅雷仓手机版","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2020-03-09 01:20:41","newno":"1446909351","nick_name":"yEs、先生","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/733416128/avatar/1537156414.jpg/300x300","province":"","pub_extra":{"uid":733416128,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2018-09-17 11:53:33","sex":"male","uid":"733416128","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"733416128","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"733416128","vasID":14,"vasType":0}]}},{"id":1019853,"url":"http://www.dygang.net/dsj/20170516/37390.htm","title":"射雕英雄传胡歌版[全集]_免费下载_国剧_电影港","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2021-08-31 11:11:12","newno":"1222957810","nick_name":"先不知后能觉","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/FmEWsWqhNrOA8W4ooJlC7XzDaUY1/300x300","province":"江苏省","pub_extra":{"uid":659666952,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-06-04 15:10:50","sex":"male","uid":"659666952","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"659666952","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"659666952","vasID":14,"vasType":0}]}},{"id":1104778,"url":"http://m.xunleicang.com/vod-read-id-28649.html","title":"射雕英雄传[黄日华版]_迅雷下载_高清电影_迅雷仓手机版","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2018-12-26 10:41:35","newno":"465422625","nick_name":"一定要幸福","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/Fiw6hVyMVFIUEqF1ebXOhhSh0AQD/300x300","province":"","pub_extra":{"uid":544862203,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2016-01-07 18:18:07","sex":"unknown","uid":"544862203","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"544862203","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"544862203","vasID":14,"vasType":0}]}},{"id":956081,"url":"http://m.xunleicang.com/vod-read-id-30656.html","title":"射雕英雄传粤语[2017版]_迅雷下载_高清电影_迅雷仓手机版","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2018-08-28 09:29:36","newno":"1278682513","nick_name":"章后聪","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/Fjq2gzutQ_V_oAS9qZjp24ljhAKw/300x300","province":"云南省","pub_extra":{"uid":690667992,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-11-09 22:31:42","sex":"male","uid":"690667992","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"690667992","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"690667992","vasID":14,"vasType":0}]}},{"id":900165,"url":"http://www.dy2018.com/i/97637.html?winzoom=1","title":"2017年大陆国产剧《射雕英雄传(2017)》全集迅雷下载_电影天堂","poster":"http://sl.wangpan.7niu.n0808.com/681f03a1828f844ef6788c579ffe1ea3f074ef3b","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2020-11-08 22:51:16","newno":"1047932871","nick_name":"撒哈拉","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/Fjry98kJQ8aG5YCSfGHSBykBXnUe/300x300","province":"河南省","pub_extra":{"uid":640448003,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-03-17 23:07:44","sex":"male","uid":"640448003","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"640448003","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"640448003","vasID":14,"vasType":0}]}},{"id":1225296,"url":"https://www.dygod.net/webPlay/play-id-108873-collection-11.html","title":"2017年大陆国产剧《射雕英雄传(2017)》连载至7 - 在线播放","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2020-11-14 14:59:12","newno":"1474607032","nick_name":"钓者 ℡¹⁸⁵⁸¹º¹¹⁷⁷⁷","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/756855436/avatar/e0iq.jpg/300x300","province":"","pub_extra":{"uid":756855436,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2019-04-04 20:18:49","sex":"male","uid":"756855436","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"756855436","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"756855436","vasID":14,"vasType":0}]}},{"id":1083138,"url":"https://m.80s.tw/movie/19624","title":"《射雕英雄传2017版》高清手机在线观看 - 80s电影网","poster":"http://sl.wangpan.7niu.n0808.com/1b4f32e97179493f8163ace8bd4d9da45e21adfe","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2018-11-27 19:44:46","newno":"1247799366","nick_name":"蒋滕","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/FoWKh28z7QbbLlZmqmyiIA4h4_mx/300x300","province":"江苏省","pub_extra":{"uid":672922343,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-08-06 00:38:53","sex":"male","uid":"672922343","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"672922343","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"672922343","vasID":14,"vasType":0}]}},{"id":973415,"url":"http://www.dy2018.com/webPlay/play-id-97637-collection-0.html","title":"2017年大陆国产剧《射雕英雄传(2017)》全集 - 在线播放","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2021-01-06 05:09:58","newno":"491048679","nick_name":"疯狂NO拽","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/FkLzTSZ3gNU1JQl6Ex1bUpYJC5dc/300x300","province":"辽宁省","pub_extra":{"uid":565274639,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2016-05-12 04:22:23","sex":"unknown","uid":"565274639","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"565274639","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"565274639","vasID":14,"vasType":0}]}},{"id":1019860,"url":"http://m.xunleicang.com/vod-read-id-9370.html","title":"新射雕英雄传[胡歌版]_迅雷下载_高清电影_迅雷仓手机版","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2021-08-31 11:11:12","newno":"1222957810","nick_name":"先不知后能觉","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/FmEWsWqhNrOA8W4ooJlC7XzDaUY1/300x300","province":"江苏省","pub_extra":{"uid":659666952,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-06-04 15:10:50","sex":"male","uid":"659666952","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"659666952","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"659666952","vasID":14,"vasType":0}]}},{"id":1104000,"url":"http://www.ygdy8.com/html/tv/hytv/20170111/52982.html","title":"2017年内地电视剧《射雕英雄传(2017)》第52集[国语字幕]","poster":"http://sl.wangpan.7niu.n0808.com/681f03a1828f844ef6788c579ffe1ea3f074ef3b","user_info":{"biography":"","birthday":"","description":"","kind":"yyh","kinds":["yyh"],"live_extra":{},"login_time":"2019-02-18 11:26:46","newno":"1449060387","nick_name":"美好生活","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/avatar/default/300x300","province":"","pub_extra":{"uid":736363192,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2018-10-19 17:30:45","sex":"unknown","uid":"736363192","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"736363192","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"736363192","vasID":14,"vasType":0}]}},{"id":1317469,"url":"http://m.xunleicang.com/vod-read-id-9940.html","title":"射雕英雄传[李亚鹏/周迅版][中日双语]_迅雷下载_高清电影_迅雷仓手机版","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2019-05-31 02:52:27","newno":"1272735487","nick_name":"夜微liang","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/FsvD2nOGniz1Ouhd__vBE2gLImGE/300x300","province":"广东省","pub_extra":{"uid":685639806,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-10-16 20:36:06","sex":"male","uid":"685639806","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"685639806","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"685639806","vasID":14,"vasType":0}]}},{"id":1036683,"url":"https://www.80s.tw/ju/19624","title":"《射雕英雄传2017版》 (2017)高清mp4迅雷下载 - 80s手机电影","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2021-10-14 13:47:46","newno":"476712922","nick_name":"混世魔王","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/FhdXc98JqVtwTbS503lozUB3CANI/300x300","province":"广东省","pub_extra":{"uid":553484568,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2016-02-28 12:54:44","sex":"male","uid":"553484568","vip_extra":[{"VIPLevel":6,"isVIP":1,"isYear":0,"uid":"553484568","vasID":2,"vasType":3},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"553484568","vasID":14,"vasType":0}]}},{"id":1040806,"url":"https://m.80s.tw/movie/3029","title":"08版射雕英雄传(2008)全集高清迅雷下载 - 80s电影网","poster":"http://sl.wangpan.7niu.n0808.com/c90a3197aa0d3f39dd535c57049ab4889b76bd5d","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2019-06-26 09:19:17","newno":"501473021","nick_name":"韦恩。","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/FokW6SbWLP9L7_U6WmPB7MIyg4ks/300x300","province":"贵州省","pub_extra":{"uid":573192600,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2016-06-26 10:22:49","sex":"unknown","uid":"573192600","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"573192600","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"573192600","vasID":14,"vasType":0}]}},{"id":1004117,"url":"http://www.80s.tw/ju/3029#","title":"《08版射雕英雄传》 (2008)高清mp4迅雷下载 - 80s手机电影","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2021-10-03 18:53:46","newno":"977664212","nick_name":"四九","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/FhquE9pbVn-P-WBOKMCSCKIPnkQ2/300x300","province":"河南省","pub_extra":{"uid":624102218,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-01-14 19:11:17","sex":"male","uid":"624102218","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"624102218","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"624102218","vasID":14,"vasType":0}]}},{"id":1360743,"url":"https://www.dygod.net/html/tv/hytv/108873.html","title":"2017年大陆国产剧《射雕英雄传(2017)》连载至7迅雷下载_电影天堂","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2019-04-24 16:59:32","newno":"1476713245","nick_name":"寻","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/758761148/avatar/lE_L.jpg/300x300","province":"","pub_extra":{"uid":758761148,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2019-04-23 19:55:55","sex":"male","uid":"758761148","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"758761148","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"758761148","vasID":14,"vasType":0}]}},{"id":1412731,"url":"https://www.dygod.net/html/gndy/jddy/20100117/24054.html?jdfwkey=7nryp2","title":"1024分辨率《射雕英雄传之东成西就》BD国语中字无水印迅雷下载_电影天堂","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2021-09-26 23:24:16","newno":"492743813","nick_name":"刘志远","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/FmDys5LdIiHtEBelku1ypvKVEsw-/300x300","province":"","pub_extra":{"uid":566584957,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2016-05-20 15:25:47","sex":"unknown","uid":"566584957","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"566584957","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"566584957","vasID":14,"vasType":0}]}},{"id":1112227,"url":"https://www.dy2018.com/i/97992.html","title":"2017年大陆国产剧《射雕英雄传2017(粤语)》连载至104迅雷下载_电影天堂","poster":"http://sl.wangpan.7niu.n0808.com/681f03a1828f844ef6788c579ffe1ea3f074ef3b","user_info":{"biography":"","birthday":"","description":"","kind":"yyh","kinds":["yyh"],"live_extra":{},"login_time":"2020-09-15 21:32:39","newno":"1216545969","nick_name":"电影天堂华语连续剧更新","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/655655590/avatar/1537877953.jpg/300x300","province":"","pub_extra":{"uid":655655590,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2016-05-19 15:21:30","sex":"unknown","uid":"655655590","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"655655590","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"655655590","vasID":14,"vasType":0}]}},{"id":893248,"url":"http://www.ygdy8.com/html/gndy/jddy/20100117/24044.html","title":"1024分辨率《射雕英雄传之东成西就》BD国语中字无水印迅雷下载_阳光电影","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2020-10-11 05:33:54","newno":"596575179","nick_name":"速度真牛B","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/Fo6OxoLWXU-AFnoh4KQ1pnJiuy7H/300x300","province":"安徽省","pub_extra":{"uid":21287927,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"","sex":"unknown","uid":"21287927","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"21287927","vasID":2,"vasType":5},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"21287927","vasID":14,"vasType":0}]}},{"id":466684,"url":"http://www.ygdy8.com/html/tv/hytv/20170111/52982.html","title":"1.ahhxwavi.cn:60/tvwm1p/76/2017年内地电视剧射雕英雄传","poster":"http://sl.wangpan.7niu.n0808.com/681f03a1828f844ef6788c579ffe1ea3f074ef3b","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2017-09-30 22:54:53","newno":"982755334","nick_name":"沉风","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/avatar/default/300x300","province":"广东省","pub_extra":{"uid":627072529,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-01-25 13:50:43","sex":"female","uid":"627072529","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"627072529","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"627072529","vasID":14,"vasType":0}]}},{"id":697314,"url":"http://www.ygdy8.com/html/tv/hytv/20170111/52982.html","title":"1.aisqaaki.cn:63/tgkxl9/76/2017年内地电视剧射雕英雄传","poster":"http://sl.wangpan.7niu.n0808.com/681f03a1828f844ef6788c579ffe1ea3f074ef3b","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2020-03-27 18:30:01","newno":"507940284","nick_name":"               囧rz","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/FkBuy9fl6pGLeRChf4IN-0srtEfz/300x300","province":"海南省","pub_extra":{"uid":578729001,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2016-07-22 16:34:11","sex":"male","uid":"578729001","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"578729001","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"578729001","vasID":14,"vasType":0}]}},{"id":1424575,"url":"https://www.dytt8.net/html/tv/hytv/20170111/52970.html?jdfwkey=sg1wi3","title":"2017年内地电视剧《射雕英雄传(2017)》第52集[国语字幕]迅雷下载_电影天堂","poster":"http://sl.wangpan.7niu.n0808.com/681f03a1828f844ef6788c579ffe1ea3f074ef3b","user_info":{"biography":"爱啥就啥吧！","birthday":"20200407","description":"爱啥就啥吧！","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2021-08-25 16:20:55","newno":"1629834640","nick_name":"凡人修仙","params":{"fav_count":2,"lastest_res_title":"","play_count":548,"play_time":0,"pub_res_count":15,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/793813125/avatar/AkzAVA.jpg/300x300","province":"辽宁省","pub_extra":{"uid":793813125,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2020-03-16 20:47:52","sex":"male","uid":"793813125","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"793813125","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"793813125","vasID":14,"vasType":0}]}},{"id":953948,"url":"http://www.piaohua.com/html/lianxuju/2017/0110/31684.html","title":"射雕英雄传下载_迅雷下载_免费下载_飘花电影网","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2018-07-04 11:30:29","newno":"1278869838","nick_name":"无声","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/Fp2aqRmWfJA5PGfh15dnIh5NuQqe/300x300","province":"广东省","pub_extra":{"uid":690813958,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-11-10 19:55:53","sex":"male","uid":"690813958","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"690813958","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"690813958","vasID":14,"vasType":0}]}},{"id":709763,"url":"http://www.ygdy8.com/html/tv/hytv/20170111/52982.html","title":"2017年内地电视剧《射雕英雄传(2017)》第52集[国语字幕]迅雷下载_阳光电影_电影天堂","poster":"http://sl.wangpan.7niu.n0808.com/681f03a1828f844ef6788c579ffe1ea3f074ef3b","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2021-10-02 18:56:39","newno":"986127823","nick_name":"Fairy_ （恶男）","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/FocyuoW4Tcdk6xnI3NXH8NP_iG5R/300x300","province":"河南省","pub_extra":{"uid":627825070,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2017-01-28 19:01:48","sex":"male","uid":"627825070","vip_extra":[{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"627825070","vasID":2,"vasType":0},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"627825070","vasID":14,"vasType":0}]}},{"id":1415950,"url":"https://www.dy2018.com/i/92643.html","title":"1993年中国香港经典喜剧片《射雕英雄传之东成西就》BD双语中字迅雷下载_电影天堂","poster":"","user_info":{"biography":"","birthday":"","description":"","kind":"per","kinds":["per"],"live_extra":{},"login_time":"2021-09-05 23:40:23","newno":"170796063","nick_name":"灬hanson灬","params":{"fav_count":0,"lastest_res_title":"","play_count":0,"play_time":0,"pub_res_count":0,"res_update_time":0,"share_count":0},"portrait_url":"https://xfile2.a.88cdn.com/file/k/avatar/default/300x300","province":"广东省","pub_extra":{"uid":315535339,"v_comment":"","v_status":0,"v_url":"http://7xnvqo.com1.z0.glb.clouddn.com/9f3d6885b3807da9018df8351f1ec254d27da2e8"},"register_date":"2014-01-29 18:01:47","sex":"unknown","uid":"315535339","vip_extra":[{"VIPLevel":4,"isVIP":0,"isYear":0,"uid":"315535339","vasID":2,"vasType":3},{"VIPLevel":0,"isVIP":0,"isYear":0,"uid":"315535339","vasID":14,"vasType":0}]}}],"total":968},"topic":{"count":1,"list":[{"topic_name":"射雕英雄传","poster":""}],"total":381},"user":{"count":0,"list":null}}`

		obj := make(map[string]interface{}, 0)

		err := json.Unmarshal([]byte(output), &obj)
		if err != nil {
			panic(err)
		}

		webutil.Return(c, obj)
	})

	r.POST("/test/return_large_string", func(c *gin.Context) {
		req := &struct {
			Input int64 `json:"input"`
		}{}

		webutil.ParseReq(c, req)

		contentBytes, err := largestring.GetLargeString()
		if err != nil {
			webutil.ReturnErr(c, err)
			return
		}

		contentLen := req.Input
		if contentLen <= 0 {
			contentLen = int64(len(contentBytes) / 4)
		}

		//从-~3/4中找一个点，取最多1/4的内容
		idxStart := randutil.RandInt(0, int64(len(contentBytes)/4*3))
		resp := &struct {
			Output string `json:"output"`
		}{Output: string(contentBytes[idxStart : idxStart+contentLen])}

		webutil.Return(c, resp)
	})

	r.GET("/test/dynamic_url/:name", func(c *gin.Context) {
		name := c.Param("name")
		resp := &struct {
			Output string `json:"output"`
		}{Output: name}

		webutil.Return(c, resp)
	})

	r.GET("/test_timeout/:sec", func(c *gin.Context) {
		secStr := c.Param("sec")
		sec, err := strconv.Atoi(secStr)
		if err != nil {
			webutil.ReturnErr(c, err)
			return
		}

		time.Sleep(time.Second * time.Duration(sec))

		webutil.ReturnOK(c)
	})
	r.POST("/test_form", func(c *gin.Context) {
		name := c.PostForm("name")
		age := c.PostForm("age")

		resp := &struct {
			Name string `json:"resp_name"`
			Age  string `json:"resp_age"`
		}{Name: name, Age: age}

		webutil.Return(c, resp)
	})
	r.POST("/test/return_number", func(c *gin.Context) {

		req := &struct {
			Input int64 `json:"input"`
		}{}

		webutil.ParseReq(c, req)

		if req.Input == 0 {
			req.Input = time.Now().UnixNano()
		}

		resp := &struct {
			Number int64 `json:"output"`
		}{Number: req.Input}

		webutil.Return(c, resp)
	})
	r.POST("/test/return_string", func(c *gin.Context) {
		type Req struct {
			Input string `json:"input"`
		}

		type Resp struct {
			Output string `json:"output"`
		}

		req := &Req{}
		err := webutil.ParseReq(c, req)
		if err != nil {
			webutil.ReturnErr(c, err)
			return
		}

		resp := &Resp{Output: req.Input}

		webutil.Return(c, resp)
	})
	r.POST("/test/send_string", func(c *gin.Context) {

		type Req struct {
			Input string `json:"input"`
		}

		type Resp struct {
			Output string `json:"output"`
		}

		req := &Req{}
		err := webutil.ParseReq(c, req)
		if err != nil {
			webutil.ReturnErr(c, err)
			return
		}

		resp := &Resp{Output: fmt.Sprintf("the input is %v", req.Input)}

		webutil.Return(c, resp)
	})
	r.POST("/test/send_int", func(c *gin.Context) {

		type Req struct {
			Input int64 `json:"input"`
		}

		type Resp struct {
			Output string `json:"output"`
		}

		req := &Req{}
		err := webutil.ParseReq(c, req)
		if err != nil {
			webutil.ReturnErr(c, err)
			return
		}

		resp := &Resp{Output: fmt.Sprintf("the input is %v", req.Input)}

		webutil.Return(c, resp)
	})
	r.POST("/test/send_float", func(c *gin.Context) {

		type Req struct {
			Input float64 `json:"input"`
		}

		type Resp struct {
			Output string `json:"output"`
		}

		req := &Req{}
		err := webutil.ParseReq(c, req)
		if err != nil {
			webutil.ReturnErr(c, err)
			return
		}

		resp := &Resp{Output: fmt.Sprintf("the input is %v", req.Input)}

		webutil.Return(c, resp)
	})
}
