package controller

import (
	"github.com/gin-gonic/gin"
	"logxxx.com/test/v2/utils/webutil"
)

type ReportReq struct {
	Action          string `json:"action"`
	IP              string `json:"ip"`
	Payload         string `json:"payload"` //create时携带链接等
	DeviceID        string `json:"device_id"`
	Platform        string `json:"platform"`         //平台类型(比如群晖)
	PlatformVersion string `json:"platform_version"` //平台版本(比如6.1;7.1)
	ClientVersion   string `json:"client_version"`   //客户端(b下载器)的版本
	OS              string `json:"os"`
}

func Report(c *gin.Context) {

	req := &ReportReq{}

	webutil.ParseReq(c, req)

	//db.save

}
