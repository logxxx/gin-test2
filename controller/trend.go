package controller

import (
	"github.com/gin-gonic/gin"
	"log"
	"logxxx.com/test/v2/utils/webutil"
)

type TrendDataReq struct {
	Key   string `json:"key"`
	Field string `json:"field"`
	Value int64  `json:"value"`
}

func ReportTrendData(c *gin.Context) {
	req := &TrendDataReq{}

	webutil.ParseReq(c, req)

	log.Printf("ReportTrendData req:%+v", req)

	save(req)

	webutil.ReturnOK(c)
}

var cache map[string]map[string]int64

func save(req *TrendDataReq) {
	if req.Key == "" {
		return
	}
}
