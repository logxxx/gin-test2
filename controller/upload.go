package controller

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"path/filepath"
)

func DownloadFile(c *gin.Context) {

}

func UploadFile(c *gin.Context) {

	file, err := c.FormFile("file")
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("FormFile err:%v", err))
		return
	}

	basePath := "./upload/"
	fileName := basePath + filepath.Base(file.Filename)

	log.Printf("存储文件名:%v", fileName)

	err = c.SaveUploadedFile(file, fileName)
	if err != nil {
		c.String(http.StatusBadRequest, fmt.Sprintf("SaveUploadedFile err:%v", err))
		return
	}

	log.Printf("存储完成")

	c.String(http.StatusOK, "SUCC")

}
