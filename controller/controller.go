package controller

import (
	"crypto/sha1"
	"encoding/hex"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"logxxx.com/test/v2/biz/robotbiz"
	"logxxx.com/test/v2/utils/log"
	"sort"
)

func makeSign(token, ts, nonce string) string {
	in := []string{token, ts, nonce}
	sort.Strings(in)

	h := sha1.New()
	for i := range in {
		h.Write([]byte(in[i]))
	}
	return hex.EncodeToString(h.Sum(nil))
}

func PostReply(c *gin.Context) {
	body, err := ioutil.ReadAll(c.Request.Body)
	if err != nil {
		log.Errorf("PostReply ReadAll err:%v", err)
		return
	}
	req, err := robotbiz.ParseReplyRequest(body)
	if err != nil {
		log.Errorf("PostReply ParseReplyRequest err:%v", err)
		return
	}
	log.Infof("PostReply ParseReplyRequest result:%+v", req)

	resp, err := robotbiz.GeneReplyMsg(req)
	if err != nil {
		log.Errorf("PostReply GeneReplyMsg err:%v", err)
		return
	}

	c.XML(200, resp)
}

func GetReply(c *gin.Context) {
	signature := c.Query("signature")
	timestamp := c.Query("timestamp")
	echostr := c.Query("echostr")
	nonce := c.Query("nonce")
	log.Infof("GetReply signature:%v timestamp:%v echostr:%v nonce:%v",
		signature, timestamp, echostr, nonce)
	serverSign := makeSign("weixin", timestamp, nonce)
	if serverSign != signature {
		c.JSON(4001001, struct {
			RequestSign string `json:"request_sign"`
			ServerSign  string `json:"server_sign"`
		}{
			RequestSign: signature,
			ServerSign:  serverSign,
		})
	} else {
		c.String(200, echostr)
	}

}

func TestJson1(c *gin.Context) {
	type Response struct {
		ErrCode   int      `json:"errorCode"`
		SessionID []string `json:"sessionID"`
		LoginKey  []string `json:"loginKey"`
	}

	resp1 := &Response{
		ErrCode:   1,
		SessionID: []string{"aa", "bb", "cc"},
		LoginKey:  []string{"dd", "ee", "ff"},
	}
	c.JSON(200, resp1)
}
