package jsonutil

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"sync/atomic"
)

// Expression Supported comparison symbols: !=, ==, >, <, >=, <=
// Supported logic symbols: &&, ||, (...)
// Constant string must begin and end with ' or ", such as "abc" or 'abc'.
// Example: toLower(my_value) == "aa" && (toUpper(my_value) == 'AA' || my_value == 'aB')
type Expression struct {
	expr string
	envs []environment

	constantMap   map[string]Value
	constantIndex int64

	functionMap   map[string]*function
	functionIndex int64
}

type function struct {
	name string
	args []string

	rootExpr *Expression
}

type environment struct {
	temp  bool
	key   string
	value string
}

// CompileExpr compile the expr to *Expression.
func CompileExpr(expr string) (*Expression, error) {
	if expr == "" {
		return nil, errors.New("empty expr")
	}
	if expr[0] == '\\' {
		return nil, errors.New("invalid expr: start with backslash")
	}

	if exprCacheSize > 0 {
		if cache, ok := exprCache.Load(expr); ok {
			return cache.(*Expression), nil
		}
	}

	ex := &Expression{expr: expr}
	err := ex.removeQuote()
	if err != nil {
		return nil, err
	}

	err = ex.parseFunction(len(ex.expr))
	if err != nil {
		return nil, err
	}

	err = ex.parseEnv()
	if err != nil {
		return nil, err
	}

	if exprCacheSize > 0 && exprCacheSize >= atomic.LoadInt32(&exprCacheCurrentSize) {
		exprCache.Store(expr, ex)
		atomic.AddInt32(&exprCacheCurrentSize, 1)
	}
	return ex, nil
}

// 去除引号包裹的常量字符串, 只能使用单引号或双引号其中的一种。
func (ex *Expression) removeQuote() error {
	src := []byte(ex.expr)
	withoutQuote := make([]byte, 0, len(src))
	begin := -1
	end := -1
	var b byte
	for i := 0; i < len(src); i++ {
		b = src[i]
		switch b {
		case '"', '\'':
			begin = i
			end = searchEndQuote(src, begin, b)
			if end < 0 {
				return errors.New("invalid expr: missing quote")
			}
			s, err := unQuote(src, begin, end)
			if err != nil {
				return fmt.Errorf("unQuote [%v] err: %v", src, err)
			}

			key := ex.addConst(NewString(s))
			withoutQuote = append(withoutQuote, key...)
			i = end
		default:
			withoutQuote = append(withoutQuote, b)
		}
	}

	ex.expr = string(withoutQuote)
	return nil

}

func (ex *Expression) parseEnv() error {
	exprs := strings.Split(strings.TrimSpace(ex.expr), ";")
	size := len(exprs)
	if size == 1 {
		return nil
	}

	ex.expr = exprs[size-1]

	for _, s := range exprs[:size-1] {
		end := len(s)
		var key, symbol, value string
		for i := 0; i < end; i++ {
			if s[i] == ':' || s[i] == '=' {
				key = strings.TrimSpace(s[:i])
				for j := i + 1; j < end; j++ {
					if s[j] != '=' {
						symbol = strings.TrimSpace(s[i:j])
						value = strings.TrimSpace(s[j:])
						break
					}
				}
				break
			}
		}

		if key == "" {
			return errors.New("no key found in ENV")
		}
		if value == "" {
			return errors.New("no value found in ENV")
		}

		switch symbol {
		case "=": // set permanently
			ex.envs = append(ex.envs, environment{
				temp:  false,
				key:   key,
				value: value,
			})
		case ":=": // set temporarily and delete when filter is done
			ex.envs = append(ex.envs, environment{
				temp:  true,
				key:   key,
				value: value,
			})
		case "":
			return errors.New("no symbol found in ENV")
		default:
			return fmt.Errorf("unsupport symbol '%v' in ENV", symbol)
		}
	}
	return nil
}

func (ex *Expression) parseFunction(scannedPos int) error {
	if ex.expr == "" {
		return errors.New("calculate empty expr")
	}

	expr := ex.expr
	if scannedPos > len(expr) {
		scannedPos = len(expr)
	}
	var bracketBegin int
	for bracketBegin = scannedPos - 1; bracketBegin > 0; bracketBegin-- {
		if expr[bracketBegin] == '(' && isValidCharForFunc(expr[bracketBegin-1]) {
			break
		}
	}

	// 无函数
	if bracketBegin <= 0 {
		return nil
	}

	// 解析函数，函数结果更新到expr里面
	nameBegin := bracketBegin - 1
	for i := nameBegin - 1; i >= 0; i-- {
		if isValidCharForFunc(expr[i]) {
			nameBegin = i
		} else {
			break
		}
	}

	funcName := string(expr[nameBegin:bracketBegin])

	bracketEnd := -1
	for i := bracketBegin + 1; i < len(expr); i++ {
		if expr[i] == ')' {
			bracketEnd = i
			break
		}
	}
	if bracketEnd < 0 {
		return errors.New("bracket not match")
	}
	args := strings.Split(strings.TrimSpace(expr[bracketBegin+1:bracketEnd]), ",")

	k := ex.addFunction(funcName, args)
	ex.expr = expr[:nameBegin] + k + expr[bracketEnd+1:]
	return ex.parseFunction(nameBegin)
}

func (ex *Expression) addConst(v Value) string {
	if ex.constantMap == nil {
		ex.constantMap = make(map[string]Value)
		ex.constantIndex = 0
	}

	ex.constantIndex++
	k := prefixConst + strconv.FormatInt(ex.constantIndex, 10)
	ex.constantMap[k] = v
	return k
}

func (ex *Expression) addFunction(name string, args []string) string {
	if ex.functionMap == nil {
		ex.functionMap = make(map[string]*function)
		ex.functionIndex = 0
	}

	ex.functionIndex++
	k := prefixFunc + strconv.FormatInt(ex.functionIndex, 10)
	f := &function{
		name:     strings.ToLower(name),
		args:     args,
		rootExpr: ex,
	}
	ex.functionMap[k] = f
	return k
}

func searchEndQuote(src []byte, begin int, quote byte) int {
	escape := false
	for i := begin + 1; i < len(src); i++ {
		if escape {
			escape = false
			continue
		}
		switch src[i] {
		case '\\':
			escape = true
			continue
		case quote:
			return i
		}
	}
	return -1
}

func unQuote(src []byte, begin, end int) (string, error) {
	// 如果是单引号，将首尾改为双引号，将内部的\'改为单引号，然后Unquote
	tmp := make([]byte, 0, end-begin+4)
	escape := false
	tmp = append(tmp, '"')
	for i := begin + 1; i < end; i++ {
		if escape {
			switch src[i] {
			case '\'':
				tmp = append(tmp, '\'')
			default:
				tmp = append(tmp, '\\', src[i])
			}
			escape = false
			continue
		}

		switch src[i] {
		case '\\':
			escape = true
		case '"':
			tmp = append(tmp, '\\', '"')
		default:
			tmp = append(tmp, src[i])
		}
	}
	if escape {
		return "", errors.New("invalid escape")
	}

	tmp = append(tmp, '"')
	return strconv.Unquote(string(tmp))
}
