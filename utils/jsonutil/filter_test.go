package jsonutil

import (
	"testing"
)

func TestJson_Filter(t *testing.T) {
	//log.SetLevel("DEBUG")
	src := `
{
  "string": "abcDEF",
  "escape": "abc \t \n \" \\ ' DEF\u263a",
  "empty": "",
  "struct": {
    "string": "hIjK",
    "number_string": "123.456789",
    "number_int": 123,
    "number_float": -123.456789,
    "bool": true,
    "array": [
      "first",
      "second",
      "third"
    ],
	"array2": [
		{"name":"hehe", "age":18},
		{"name":"haha", "age":19}
	]
  }
}
`
	//		{"struct.array2[0].name=='hehe'", True},

	j, err := NewJSON([]byte(src))
	if err != nil {
		t.Fatal(err)
	}
	bs, _ := j.Marshal()
	_ = bs
	t.Logf("INFO json: %v", bs)

	testCase := []struct {
		expr string
		ret  Result
	}{

		// "escape": "abc \t \n \" \\ ' DEF",
		// ==, =
		{"struct!=null", True},
		{"struct==null", False},
		{"struct.bool == true", True},
		{"struct.bool == false", False},
		{"struct.bool > false", Error},
		{"toLower(string) = 'abcdef'", Error}, // cancel '='
		{"toLower(string) == 'abcdef'", True},
		{"toLower(string) == \"abcdef\"", True},
		{"toLower(escape) == \"abc \\t \\n \\\" \\\\ ' def\\u263a\"", True},
		{`toLower(escape) == 'abc \t \n " \\ \' def\u263a'`, True},
		{`toLower(escape) == 'abc \t \n " \\ \' def☺'`, True},
		{`toLower(escape) == "abc \t \n \" \\ \' def☺"`, True},
		{`toLower(escape) == "abc \t \n \" \\ \' def☺" && toLower(string) == 'abcdef' && toLower(string) == "abcdef"`, True},
		{`toLower(escape) == 'abc \t \n " \\ \' def☺\ '`, Error},
		{"sum(struct.number_string, struct.number_float, 1) == 1.0", True},
		{"sum(struct.number_string, struct.number_float, 1) == 1.1", False},
		{"struct.number_int == 123", True},
		{"struct.number_int == '123'", True},
		{"struct.number_int != 1234", True},
		{"struct.number_int != 'xxx'", True},

		// !=
		{"toLower(string) != 'ab'", True},
		{"sum(struct.number_string, struct.number_float, 1) != 2", True},
		{"sum(struct.number_string, struct.number_float, 1) != 1", False},
		// <=
		{"toLower(string) <= 'ac'", True},
		{"sum(struct.number_string, struct.number_float, 1) <= 1.000001", True},
		{"sum(struct.number_string, struct.number_float, 1) <= 0.999999", False},
		// >=
		{"toLower(string) >= 'aa'", True},
		{"sum(struct.number_string, struct.number_float, 1) >= 1", True},
		{"sum(struct.number_string, struct.number_float, 1) >= 2", False},
		// <
		{"toLower(string) < 'ac'", True},
		{"sum(struct.number_string, struct.number_float, 1) < 2", True},
		{"sum(struct.number_string, struct.number_float, 1) < 1", False},
		// >
		{"toLower(string) > 'aa'", True},
		{"sum(struct.number_string, struct.number_float, 1) > 0.999999", True},
		{"sum(struct.number_string, struct.number_float, 1) > 1.000001", False},

		// || &&
		{"toLower(string) > 'aa' && string=='abcDEF'", True},
		{"toLower(string) < 'aa' || string!='abcDEF'", False},
		// ENV :=
		{"string := toLower(string) ; string == 'abcdef'", Error},
		{"newString := toLower(string);newString == 'abcdef'", True},
		{"newString1 := toLower(string);newString == 'abcdeF'", False},
		{"new.struct:=struct; new.struct.number_float == -123.456789", True},
		{"new.struct1:=struct; new.struct1.number_float == -223.456789", False},

		{"tmp1:=toLower(string); tmp2:=toUpper(string); tmp3:=string; tmp1==toLower(string) && tmp2==toUpper(string) && tmp3==string", True},
		{"tmp1:=toLower(string); tmp2:=toUpper(string); tmp3:=string; tmp1==toLower(string) && tmp2==toUpper(string) && tmp3!=string", False},

		// ENV =
		{"new1.struct1=struct; new1.struct1.number_float == -123.456789", True},
		{"new2.struct2=struct; new2.struct2.number_float == -223.456789", False},
		{"tmp1=toLower(string); tmp2=toUpper(string); tmp3=string; tmp1==toLower(string) && tmp2==toUpper(string) && tmp3==string", True},
		{"tmp1=toLower(string); tmp2=toUpper(string); tmp3=string; tmp1==toLower(string) && tmp2==toUpper(string) && tmp3!=string", False},

		// test parse bracket
		{"(string=='abcDEF'", Error},
		{"(string=='abcDEF))'", Error},
		{"toLower(string) > 'aa' || (toLower(string) < 'aa' || string!='abcDEF')", True},
		{"toLower(string) < 'aa' || (toLower(string) < 'aa' || string!='abcDEF')", False},
		{"(toLower(string) < 'aa' && toLower(string) > 'aa') || string=='abcDEF'", True},
		{"(toLower(string) < 'aa' && toLower(string) > 'aa') || string!='abcDEF'", False},
		{"(toLower(string) < 'aa' || (toLower(string) > 'a') && (hasPrefix(string, 'ab') == true || hasPrefix(string, 'AB') == true)) || string!='abcDEF'", True},
		{"(toLower(string) < 'aa' || (toLower(string) > 'a') && (hasPrefix(string, 'ab') == true && hasPrefix(string, 'AB') == true)) || string!='abcDEF'", False},
		{"(toLower(string) < 'aa' || (toLower(string) > 'a') && (hasPrefix(string, 'ab') == true || hasPrefix(string, 'AB') == true)) && string!='abcDEF'", False},
		{"(toLower(string) < 'aa' || (toLower(string) > 'a') && (hasPrefix(string, 'ab') == false || hasPrefix(string, 'AB') == true)) || string!='abcDEF'", False},
		{"length(struct.array)>0", True},
		{"struct.array[0]=='first'", True},
		{"struct.array[1]=='first'", False},
		{"struct.array2[0].name=='hehe'", True},
		{"elem:=struct.array2[0];elem.name=='hehe'", True},
		{"length(struct.array2)==2", True},
		// regexp match
		{"regexMatch('^[0-9.-]+$', struct.number_float) == true", True},
		{"regexMatch('^[0-1]+$', struct.number_float) == true", False},

		// http functions, should only test in local env.
	}

	for _, v := range testCase {
		ret, err := j.Filter(v.expr)
		_ = err
		t.Logf("INFO expr:%v, result:%v, err:%v", v.expr, ret, err)
		//t.Logf("INFO expr:%v, result:%v, err:%v, json:%v", v.expr, ret, err, j.MarshalStringPretty())
		if ret != v.ret {
			t.Fatalf("ERROR: expr:%v, expect result:%v, get:%v, json:%v", v.expr, v.ret, ret, j.MarshalString())
		}
	}

	//j,err=NewJSON([]byte(mylog))
	//if err!=nil{
	//	fmt.Println("newjson err",err.Error())
	//	return
	//}
	//rule1 :="labels.app=='ingress-nginx'"
	//rule2 := "mylog:=decodejson(log);mylog.remote_addr=='100.109.44.107'"
	//if ret,err:=j.Filter(rule1);err==nil&&ret==True {
	//	if ret, err = j.Filter(rule2); err!=nil || ret == False {
	//		t.Fatalf("not match rule2")
	//	}
	//} else {
	//	t.Fatalf("not match rule1")
	//}
	rule3 := `myjson:=decodejson('{"myname":"abner"}');myjson.myname=='abner'`
	if ret, err := j.Filter(rule3); err == nil && ret == False {
		t.Fatalf("not match rule3")
	}
}

func BenchmarkFilterPathDepth1(b *testing.B) {
	src := `
{
  "string": "hIjK",
  "number_int": 123,
  "number_float": -123.456789,
  "bool": true,
  "array": [
    "first",
    "second",
    "third"
  ],
  "struct": {
    "string": "hIjK",
    "number_int": 123,
    "number_float": -123.456789,
    "bool": true,
    "array": [
      "first",
      "second",
      "third"
    ]
  }
}
`
	j, err := NewJSON([]byte(src))
	if err != nil {
		b.Fatal(err)
	}
	expr := "toUpper(string) == 'HIJK' && add(number_int, 100) == 223 && sum(number_float, 123.456789) == 0 && bool == true && (array[1] == 'second' || array[1] == 'ERROR')"
	if r, _ := j.Filter(expr); r != True {
		b.Fatal("result not match")
	}

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			j.Filter(expr)
		}
	})
}

func BenchmarkFilterPathDepth2(b *testing.B) {
	src := `
{
  "string": "hIjK",
  "number_int": 123,
  "number_float": -123.456789,
  "bool": true,
  "array": [
    "first",
    "second",
    "third"
  ],
  "struct": {
    "string": "hIjK",
    "number_int": 123,
    "number_float": -123.456789,
    "bool": true,
    "array": [
      "first",
      "second",
      "third"
    ]
  }
}
`
	j, err := NewJSON([]byte(src))
	if err != nil {
		b.Fatal(err)
	}
	expr := "toUpper(struct.string) == 'HIJK' && add(struct.number_int, 100) == 223 && sum(struct.number_float, 123.456789) == 0 && struct.bool == true && (struct.array[1] == 'second' || struct.array[1] == 'ERROR')"
	if r, _ := j.Filter(expr); r != True {
		b.Fatal("result not match")
	}

	b.ResetTimer()
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			j.Filter(expr)
		}
	})
}
