package jsonutil

import (
	"encoding/json"
	"testing"
)

func TestNewValue(t *testing.T) {
	testCase := []struct {
		val interface{}
		tpe VType
	}{
		{nil, TypeNil},
		{"", TypeString},
		{"string", TypeString},
		{struct {
			S string
			I int
		}{
			S: "string",
			I: 1,
		}, TypeJSON},
		{true, TypeBool},
		{false, TypeBool},
		{int(-1), TypeNumber},
		{int8(-1), TypeNumber},
		{uint(1), TypeNumber},
		{uint8(1), TypeNumber},
		{float32(1.11), TypeNumber},
		{float64(-1.11), TypeNumber},
		//{json.Number("1.11"), TypeNumber},
		{json.Number("1.11"), TypeString},
	}

	for _, v := range testCase {
		dst, err := NewValue(v.val)
		if err != nil {
			t.Fatalf("src type:%T, src value:%v, err:%v", v.val, v.val, err)
		}

		//t.Logf("INFO: src type:%T, src value:%+v, dst.value:%v, dst.type:%v", v.val, v.val, dst, dst.t)

		if dst.t != v.tpe {
			t.Fatal("type not match")
		}
	}
}
