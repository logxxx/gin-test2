package jsonutil

import (
	"fmt"
	"reflect"
	"strconv"
)

// Value define a basic value and type, the type should be one of nil, string, bool, number.
type Value struct {
	v interface{}
	t VType
}

//VType Value type
type VType int8

// define all types.
const (
	TypeNil    = VType(0)
	TypeString = VType(1)
	TypeBool   = VType(2)
	TypeNumber = VType(3)
	TypeJSON   = VType(4)
)

var (
	// Nil is a nil value, return when some key not found in json.
	Nil = Value{
		v: nil,
		t: TypeNil,
	}
)

// NewValue return a value from v, auto detected the type of value.
func NewValue(v interface{}) (Value, error) {
	if v == nil {
		return Nil, nil
	}

	rv := reflect.Indirect(reflect.ValueOf(v))
	k := rv.Kind()
	switch k {
	case reflect.String:
		return NewString(rv.String()), nil
	case reflect.Bool:
		return NewBool(rv.Bool()), nil
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return NewNumber(float64(rv.Int())), nil
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64:
		return NewNumber(float64(rv.Uint())), nil
	case reflect.Float32, reflect.Float64:
		return NewNumber(rv.Float()), nil

	default:
		if k == reflect.Struct {
			t := rv.Type()
			if t == typeValue {
				val := rv.Interface().(Value)
				return val, nil
			}
			if t == typeJSON {
				val := rv.Interface().(JSON)
				return NewJSONValue(val), nil
			}

		}
		val, err := NewJSONFromInterface(v)
		if err != nil {
			return Nil, err
		}
		return NewJSONValue(*val), nil
	}
}

// NewString return a string value from v.
func NewString(v string) Value {
	return Value{
		v: v,
		t: TypeString,
	}
}

// NewNumber return a number value from v.
func NewNumber(v float64) Value {
	return Value{
		v: v,
		t: TypeNumber,
	}
}

// NewBool return a bool value from v.
func NewBool(v bool) Value {
	return Value{
		v: v,
		t: TypeBool,
	}
}

// NewJSONValue return a JSON value from v.
func NewJSONValue(v JSON) Value {
	return Value{
		v: v,
		t: TypeJSON,
	}
}

// String return the string format of value.
func (v Value) String() string {
	switch v.t {
	case TypeString:
		return v.v.(string)
	case TypeNil:
		return ""
	case TypeNumber:
		return strconv.FormatFloat(v.v.(float64), 'f', -1, 64)
	case TypeBool:
		if v.v.(bool) {
			return "true"
		}
		return "false"
	case TypeJSON:
		tmp := v.v.(JSON)
		return tmp.MarshalString()
	default:
		return fmt.Sprintf("%v", v.v)
	}
}

// Number return the float64 format of value.
func (v Value) Number() (float64, error) {
	switch v.t {
	case TypeString:
		return parseFloat(v.v.(string))
	case TypeBool:
		if v.v.(bool) {
			return 1, nil
		}
		return 0, nil
	case TypeNumber:
		return v.v.(float64), nil
	case TypeNil:
		return 0, nil
	default:
		return 0, fmt.Errorf("type %v can't convert to number", v.t)
	}
}

// Int return the int64 format of value.
func (v Value) Int() (int64, error) {
	switch v.t {
	case TypeString:
		return parseInt(v.v.(string))
	case TypeBool:
		if v.v.(bool) {
			return 1, nil
		}
		return 0, nil
	case TypeNumber:
		return int64(v.v.(float64)), nil
	case TypeNil:
		return 0, nil
	default:
		return 0, fmt.Errorf("type %v can't convert to int", v.t.String())
	}
}

// Bool return the bool format of value.
func (v Value) Bool() (bool, error) {
	switch v.t {
	case TypeString:
		return v.v.(string) == "true", nil
	case TypeBool:
		return v.v.(bool), nil
	case TypeNumber:
		return v.v.(float64) == 1, nil
	case TypeNil:
		return false, nil
	default:
		return false, fmt.Errorf("type %v can't convert to bool", v.t.String())
	}
}

// JSON return the JSON struct of value.
func (v Value) JSON() (JSON, error) {
	switch v.t {
	case TypeJSON:
		return v.v.(JSON), nil
	case TypeNil:
		return *New(), nil
	default:
		return *New(), fmt.Errorf("type %v cant't convert to JSON", v.t.String())
	}
}

// Type return the type of value.
func (v Value) Type() VType {
	return v.t
}

func (t VType) String() string {
	switch t {
	case TypeNil:
		return "nil"
	case TypeString:
		return "string"
	case TypeBool:
		return "bool"
	case TypeNumber:
		return "number"
	case TypeJSON:
		return "json"
	default:
		return "unknown"
	}
}
