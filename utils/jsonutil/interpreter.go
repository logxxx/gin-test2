package jsonutil

import (
	"encoding/json"
)

// Interpreter define an interface to marshal or unmarshal json.
type Interpreter interface {
	Marshal(v interface{}) ([]byte, error)
	Unmarshal(data []byte, v interface{}) error
}

// StdInterpreter is an Interpreter with encoding/json.
type StdInterpreter struct {
}

// Marshal return the JSON encoding of v.
func (i StdInterpreter) Marshal(v interface{}) ([]byte, error) {
	return json.Marshal(v)
}

// Unmarshal decode the JSON encoding data to a interface{}.
func (i StdInterpreter) Unmarshal(data []byte, v interface{}) error {
	return json.Unmarshal(data, v)
}

var interpreter = Interpreter(StdInterpreter{})

// SetInterpreter set the interpreter for this pkg.
// default is StdInterpreter.
func SetInterpreter(it Interpreter) {
	interpreter = it
}
