package jsonutil

import (
	"errors"
	"fmt"
	"logxxx.com/test/v2/utils/log"
	"strings"
)

// Result is the result type of func Filter.
type Result int

const (
	// True : pass filter
	True = Result(1)
	// False : do not pass filter
	False = Result(0)
	// Error : error occur when check filter
	Error = Result(-1)
)

// Filter check whether data in json could meet the filter condition.
// Supported comparison symbols: !, !=, =, ==, >, <, >=, <=
// Supported logic symbols: &&, ||, (...)
// Constant string must begin and end with ' or ", such as "abc" or 'abc'.
// Example: toLower(my_value) == "aa" && (toUpper(my_value) == 'AA' || my_value == 'aB')
func (j *JSON) Filter(exprs ...string) (ret Result, err error) {
	if j == nil {
		return Error, ErrNilJSON
	}
	if len(exprs) == 0 {
		return Error, errors.New("no expr specified")
	}

	exps := make([]*Expression, 0, len(exprs))
	for _, expr := range exprs {
		ex, err := CompileExpr(expr)
		if err != nil {
			return Error, err
		}
		exps = append(exps, ex)
	}
	return j.FilterExprs(exps...)
}

// FilterExprs do filter with compiled exprs.
func (j *JSON) FilterExprs(exps ...*Expression) (ret Result, err error) {
	defer func() {
		// 稳定后删除
		if e := recover(); e != nil {
			log.Errorf("recover error:%v", e)
			ret = Error
			err = fmt.Errorf("%v", e)
		}
	}()

	for _, exp := range exps {
		r, e := j.filter(exp)
		if r != True {
			return r, e
		}
	}
	return True, nil
}

func (j *JSON) filter(ex *Expression) (Result, error) {
	tmpKeys, err := j.calcEnv(ex)
	defer func() {
		for _, k := range tmpKeys {
			j.Del(k)
		}
	}()
	if err != nil {
		return Error, err
	}

	expr, err := j.removeLogicBracket(ex.expr, ex)
	if err != nil {
		return Error, err
	}
	return j.calcAndOr(expr, ex)
}

func (j *JSON) calcEnv(expr *Expression) (tmpKeys []string, err error) {
	for _, env := range expr.envs {
		// 临时变量不能与json里面的key冲突
		if env.temp {
			tmp, _ := j.Get(env.key)
			if tmp != Nil {
				return nil, errors.New("temporary ENV `" + env.key + "` is already exists in json")
			}
			tmpKeys = append(tmpKeys, env.key)
		}

		val, err := j.getWithSyntax(env.value, expr)
		if err != nil {
			return nil, err
		}
		err = j.Set(env.key, val)
		if err != nil {
			return nil, err
		}
	}
	return tmpKeys, nil
}

func isSymbol(b byte) bool {
	switch b {
	case '=', '>', '<', '!':
		return true
	default:
		return false
	}
}

//calcAndOr '&&' has a higher priority than '||'.
func (j *JSON) calcAndOr(expr string, rootExpr *Expression) (Result, error) {
	spOR := strings.SplitN(expr, "||", 2)

	if len(spOR) < 2 {
		spAND := strings.SplitN(expr, "&&", 2)
		if len(spAND) < 2 {
			return j.calcSimpleFilter(expr, rootExpr)
		}

		ret, err := j.calcSimpleFilter(spAND[0], rootExpr)
		if err != nil {
			return Error, err
		}
		if ret == False {
			return ret, nil
		}
		return j.calcAndOr(spAND[1], rootExpr)
	}

	ret, err := j.calcAndOr(spOR[0], rootExpr)
	if err != nil {
		return Error, err
	}
	if ret == True {
		return ret, nil
	}
	return j.calcAndOr(spOR[1], rootExpr)
}

// calcSimpleFilter no logic bracket in the expr.
func (j *JSON) calcSimpleFilter(expr string, rootExpr *Expression) (Result, error) {
	expr = strings.TrimSpace(expr)
	if expr == "" {
		return Error, errors.New("empty expr")
	}

	switch strings.ToLower(expr) {
	case strFalse:
		return False, nil
	case strTrue:
		return True, nil
	}

	k := ""
	e := ""

	for i := 0; i < len(expr); i++ {
		if isSymbol(expr[i]) {
			k = strings.TrimSpace(expr[:i])
			e = strings.TrimSpace(expr[i:])
			break
		}
	}
	if k == "" {
		return Error, fmt.Errorf("no symbol found in '%v'", expr)
	}
	if e == "" {
		return Error, fmt.Errorf("no value found in '%v'", expr)
	}

	var symbol string
	var value string
	var i int
	for i = 0; i < len(e); i++ {
		if !isSymbol(e[i]) {
			symbol = e[:i]
			value = strings.TrimSpace(e[i:])
			break
		}
	}
	if symbol == "" {
		return Error, fmt.Errorf("no comparison symbol found in expr '%v'", expr)
	}
	if value == "" {
		return Error, fmt.Errorf("no comparison target found in expr '%v'", expr)
	}

	src, err := j.getWithSyntax(k, rootExpr)
	if err != nil {
		return Error, err
	}

	dst, err := j.getWithSyntax(value, rootExpr)
	if err != nil {
		return Error, err
	}

	tp := src.Type()
	dtp := dst.Type()

	// 两边都是bool类型时, 只能用等于或不等于
	if tp == TypeBool && dtp == TypeBool && symbol != "==" && symbol != "!=" {
		return Error, fmt.Errorf("type mismatch in expr:%v", expr)
	}

	// 除非表达式左右两边都是Number才会转为Number进行比较
	// 其它情况使用string进行比较
	switch symbol {
	case "==":
		if tp == TypeNumber && dtp == TypeNumber {
			return newResult(src.v.(float64) == dst.v.(float64)), nil
		}
		return newResult(src.String() == dst.String()), nil
	case "!=":
		if tp == TypeNumber && dtp == TypeNumber {
			return newResult(src.v.(float64) != dst.v.(float64)), nil
		}
		return newResult(src.String() != dst.String()), nil
	case "<=":
		if tp == TypeNumber && dtp == TypeNumber {
			return newResult(src.v.(float64) <= dst.v.(float64)), nil
		}
		return newResult(src.String() <= dst.String()), nil
	case ">=":
		if tp == TypeNumber && dtp == TypeNumber {
			return newResult(src.v.(float64) >= dst.v.(float64)), nil
		}
		return newResult(src.String() >= dst.String()), nil
	case "<":
		if tp == TypeNumber && dtp == TypeNumber {
			return newResult(src.v.(float64) < dst.v.(float64)), nil
		}
		return newResult(src.String() < dst.String()), nil
	case ">":
		if tp == TypeNumber && dtp == TypeNumber {
			return newResult(src.v.(float64) > dst.v.(float64)), nil
		}
		return newResult(src.String() > dst.String()), nil

	case "=", "!":
		return Error, fmt.Errorf("symbol '=','!' had been canceled")
	}

	return Error, fmt.Errorf("unknown comparison symbol: %v", symbol)
}

// removeLogicBracket remove only logic brackets, ignore function brackets.
func (j *JSON) removeLogicBracket(expr string, rootExpr *Expression) (string, error) {
	expr = strings.TrimSpace(expr)
	//log.Debug("calcExpr:", expr)

	length := len(expr)
	copyExpr := []byte(expr)
	begin := -1

	var c1, c2 int
	for i, ch := range copyExpr {
		switch ch {
		case '(':
			c1++
			begin = i
		case ')':
			c2++
		}
	}
	if c1 != c2 {
		return "", errors.New("bracket mismatch")
	}
	if c1 == 0 {
		return expr, nil
	}

	for ; begin >= 0; begin-- {
		if copyExpr[begin] == '(' {
			end := begin + 1
			for ; end < length; end++ {
				if copyExpr[end] == ')' {
					break
				}
			}
			if end >= length {
				return "", errors.New("missing close bracket")
			}

			if begin == 0 || !isValidCharForFunc(expr[begin-1]) { // this is a logic bracket
				r, err := j.calcAndOr(expr[begin+1:end], rootExpr)
				if err != nil {
					return "", err
				}
				return j.removeLogicBracket(expr[:begin]+r.String()+expr[end+1:], rootExpr)
			}

			// this is a function bracket
			copyExpr[begin] = ' '
			copyExpr[end] = ' '
		}
	}
	return expr, nil
}

func newResult(b bool) Result {
	if b {
		return True
	}
	return False
}

// String return result as string.
func (r Result) String() string {
	if r == True {
		return "true"
	}
	if r == False {
		return "false"
	}
	return "error"
}
