## jsonutil

### 使用示例

``` go
package main

import (
    "fmt"
)

func main() {
    // asume you get a msg like this:
    msg := `{"firstName":"LeBron", "lastName":"James", "info":{"age":"33", "gender":"man"}}`

    // init to a json
    j, _ := jsonutil.NewJSON([]byte(msg))

    // get first name
    firstName, _ := j.Get("firstName")
    fmt.Println("first name:", firstName)

    // get full name
    fullName, _ := j.Calculate("append(firstName, ' ', lastName)")
    fmt.Println("full name:", fullName)

    // get age
    age, _ := j.Get("info.age")
    fmt.Println("age:", age)

    // get age after 10 years
    age10, _ := j.Calculate("add(info.age, 10)")
    fmt.Println("age after 10 years:", age10)

    // check if gender is man
    ret, _ := j.Filter("info.gender == 'man'")
    if ret == jsonutil.True {
        fmt.Println(firstName, "is a man")
    } else {
        fmt.Println(firstName, "is not a man")
    }

    // check is age greater than 30 and gender is man
    ret, _ = j.Filter("info.age > 30 && info.gender == 'man'")
    if ret == jsonutil.True {
        fmt.Println("age greater than 30 and gender is man")
    }
}

//For more examples, take a look at the go-test files.
```

### 注意事项

json字段名以及注册函数命名规范
* 不能使用`数字`开头
* 不能命名为`true`, `false`
* 仅支持使用以下字符: `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_@./`

### Calculate, Filter接口支持的函数

函数名不区分大小写

| 函数名 | 参数个数 | 参数类型 | 作用 |
|--------|--------|--------|--------|
|toNumber |1 |any | 转换为float64类型，转换失败结果为0 |
|int |1 |any | 转换为int64类型(实际依然是float64，只是去掉了小数位)，转换失败结果为0 |
|max |>=1 |float64 | 获取最大数 |
|min |>=1 |float64 | 获取最小数 |
|average |>=1|float64 | 获取平均数 |
|sum |>=1 |float64 |多个数求和 |
|add |2 |float64 |两个数相加 |
|subtract |2 |float64 |两个数相减 |
|multiply |2 |float64 |两个数相乘 |
|divide |2 |float64 |两个数相除 |
|mod |2 |int64 |两个数取余 |
|length |1 |any | 转换为string类型后的长度 |
|toUpper |1 |any | 转换为大写string类型 |
|toLower |1 |any | 转换为小写string类型 |
|subStr |2或3 |any,int[,int] | 获取子串，下标从0开始. <br>第2个参数表示起始位置，第3个参数表示结束位置(不传表示取到末尾) |
|hasPrefix |2 |string | 判断第2个字符串是否是第一个字符的前缀|
|hasSuffix |2 |string | 判断第2个字符串是否是第一个字符的后缀|
|contains |2 |string | 判断第2个字符串是否是第一个字符的子串|
|replace |3 |string/float64/json, string, string | 第一个参数为源数据，可以是字符串、数字或JSON结构，第二个字符串是待查找的字符串，第三个字符串是替换为的字符串，返回替换完成后的`字符串`|
|append | >=2 |string | 字符串拼接|
|formatString |>=1 |string,any...| 字符串格式化，golang格式，类型不确定的情况下尽量使用`%v`，示例: `formatstring('p1=%v&p2=%v', key1, key2')`, int类型实际存储为为float64，可以用`%v`或`%.f`输出|
|printString |>=1 |string,any...| 类似formatString，同时会将结果输出到stdout |
|formatTime |3 |string| 时间格式化, 参数如下: <br>第一个参数为时间字符串, 若为`now`表示当前时间 <br>第二个参数为源时间格式 <br>第三个参数为输出目标格式, 若为空则默认使用RFC3339格式`2006-01-02T15:04:05Z07:00` <br>支持的格式有: `unix-s,unix-ms,unix-us,unix-ns以及golang格式(例如: 2006-01-02T15:04:05Z07:00)`
|formatLocalTime |3 |string| 时间格式化, 默认输入时间时区为本地时区, 用法同上|
|regexMatch |2 |string| 正则匹配，第一个参数为正则表达式，第2个参数为待匹配的字符串, 返回true或false |
|regexFindAll |2 |string| 正则匹配，第一个参数为正则表达式，第2个参数为待匹配的字符串, 返回所有查找到的字符串数组 |
|regexReplaceAll |3 |string| 正则替换，第一个参数为正则表达式，第2个参数为待匹配的字符串, 第三个为待替换的字符串, 返回替换后的字符串 |
|httpGet |1|string|参数为url，返回结果为json格式：json包含如下key: <br>`statusCode`: http状态码<br>`status`: 状态描述<br>`rawBody`: 原始返回报文<br>`body`: 返回报文解析为json后的结果(解析失败为空)<br>`header`: 返回http头,json格式|
|httpPost| 3 | string,string,string |参数分别为: url, contentType, body, 返回结果参考httpGet. |
|decodeJSON |1|string,json|参数为待解析的json字符串或已经是json的类型，返回结果为json类型|

## Filter接口支持的逻辑运算

* 比较符: ` !=, ==, >, <, >=, <= `
* 逻辑运算: ` &&, ||`
* 优先级: ` 小括号 `
* 常规字符串标识: ` 单引号或双引号 `

### 性能测试

测试json数据:

``` json
{
  "string": "hIjK",
  "number_int": 123,
  "number_float": -123.456789,
  "bool": true,
  "array": [
    "first",
    "second",
    "third"
  ],
  "struct": {
    "string": "hIjK",
    "number_int": 123,
    "number_float": -123.456789,
    "bool": true,
    "array": [
      "first",
      "second",
      "third"
    ]
  }
}
```

测试filter表达式:

* BenchmarkFilterPathDepth1(json单层结构): `toUpper(string) == 'HIJK' && add(number_int, 100) == 223 && sum(number_float, 123.456789) == 0 && bool == true && (array[1] == 'second' || array[1] == 'ERROR')`

* BenchmarkFilterPathDepth2(json两层结构): `toUpper(struct.string) == 'HIJK' && add(struct.number_int, 100) == 223 && sum(struct.number_float, 123.456789) == 0 && struct.bool == true && (struct.array[1] == 'second' || struct.array[1] == 'ERROR')`


测试结果(2020-01-09-v0.9.50):

```
JSON_UTIL_CACHE_SIZE=100 go test -v -bench=. -run=none -cpu=1,4 -benchtime=1s *.go
goos: darwin
goarch: amd64
BenchmarkFilterPathDepth1         300000              4353 ns/op
BenchmarkFilterPathDepth1-4      1000000              1234 ns/op
BenchmarkFilterPathDepth2         300000              4862 ns/op
BenchmarkFilterPathDepth2-4      1000000              1366 ns/op
BenchmarkJSONSet                20000000                78.7 ns/op
BenchmarkJSONSet-4              20000000                76.3 ns/op
BenchmarkMapSet                 200000000                9.70 ns/op
BenchmarkMapSet-4               200000000                9.75 ns/op
BenchmarkJSONGet                20000000                64.4 ns/op
BenchmarkJSONGet-4              20000000                61.7 ns/op
BenchmarkMapGet                 100000000               17.8 ns/op
BenchmarkMapGet-4               100000000               17.8 ns/op
BenchmarkNewJSON                  200000              8439 ns/op
BenchmarkNewJSON-4                200000              8464 ns/op
PASS
ok      command-line-arguments  24.523s
```
