package jsonutil

import (
	"encoding/json"
	"fmt"
	"testing"
)

func TestNewJSON(t *testing.T) {
	src := []byte(`
{
  "dep1.dep2.dep3.a": "a",
  "dep1.dep2.dep3": {
    "b": "b"
  },
  "dep1.dep2": {
    "dep3.c": "c",
    "dep3": {
      "d": "d"
    }
  },
  "dep1": {
    "dep2.dep3.e": "e",
    "dep2.dep3": {
      "f": "f"
    },
    "dep2": {
      "dep3": {
        "g": "g"
      }
    }
  },
  "array": [
    {
      "dep1.dep2.dep3.a": "a"
    },
    {
      "dep1.dep2.dep3": {
        "b": "b"
      }
    },
    {
      "dep1.dep2": {
        "dep3.c": "c",
        "dep3": {
          "d": "d"
        }
      }
    },
    {
      "dep1": {
        "dep2.dep3.e": "e",
        "dep2.dep3": {
          "f": "f"
        },
        "dep2": {
          "dep3": {
            "g": "g"
          }
        }
      }
    }
  ]
}
`)

	dst := []byte(`
{
  "array": [
    {
      "dep1": {
        "dep2": {
          "dep3": {
            "a": "a"
          }
        }
      }
    },
    {
      "dep1": {
        "dep2": {
          "dep3": {
            "b": "b"
          }
        }
      }
    },
    {
      "dep1": {
        "dep2": {
          "dep3": {
            "c": "c",
            "d": "d"
          }
        }
      }
    },
    {
      "dep1": {
        "dep2": {
          "dep3": {
            "e": "e",
            "f": "f",
            "g": "g"
          }
        }
      }
    }
  ],
  "dep1": {
    "dep2": {
      "dep3": {
        "a": "a",
        "b": "b",
        "c": "c",
        "d": "d",
        "e": "e",
        "f": "f",
        "g": "g"
      }
    }
  }
}`)
	j, err := NewJSON(src)
	if err != nil {
		t.Fatal("NewJSON error:", err)
	}

	srcBytes, err := json.Marshal(j.Interface())
	if err != nil {
		t.Fatal("Marshal to json error:", err)
	}

	dstMap := make(Map)
	_ = json.Unmarshal(dst, &dstMap)
	dstBytes, _ := json.Marshal(dstMap)

	if string(srcBytes) != string(dstBytes) {
		t.Fatal("result not match")
	}
}

func TestNewJSONFromInterface(t *testing.T) {
	m := map[string]interface{}{
		"string": "abc",
		"number": 1.1,
		"bool":   true,
		"array":  []int{1, 2, 3},
		"map": map[string]string{
			"abc": "abc",
			"def": "def",
		},
		"struct": struct {
			S string `json:"s"`
			I int    `json:"i"`
		}{S: "str", I: 1},
	}

	j, err := NewJSONFromInterface(m)
	if err != nil {
		t.Fatal(err)
	}
	for _, k := range []string{"string", "number", "bool", "array[1]", "array[3]", "map.abc", "struct.s"} {
		v, err := j.Get(k)
		_ = v
		if err != nil {
			t.Fatal(err)
		}
		//t.Logf("key:%v, value:%v, type:%v", k, v, v.Type())
	}
}

func TestJSON_Get(t *testing.T) {
	m := map[string]interface{}{
		"string": "abc",
		"number": 1.1,
		"bool":   true,
		"array":  []int{1, 2, 3},
		"map": map[string]string{
			"abc": "abc",
			"def": "def",
		},
		"struct": struct {
			S string `json:"s"`
			I int    `json:"i"`
		}{S: "str", I: 1},
	}

	j, err := NewJSONFromInterface(m)
	if err != nil {
		t.Fatal(err)
	}

	t.Log("json:", j.MarshalString())

	testCase := []struct {
		key string
		val interface{}
		err error
	}{
		{"string", "abc", nil},
		{"number", "1.1", nil},
		{"bool", "true", nil},
		{"array[1]", "2", nil},
		{"array[3]", "", nil},
		{"map.abc", "abc", nil},
		{"struct.s", "str", nil},

		{"S", "", nil},
		{"S.s", "", nil},
		{"SS", "", nil},
		{"SS.SS.SS", "", nil},
		{"SS.SS.SS[3].SS", "", nil},

		{"SS.1z", "", ErrInvalidKey},
		{"struct.s.x.y.z", "", ErrTypeAssertion},
		{"struct.S.x.y.z", "", nil}, // Nil
		{"struct.S.x.y.1z", "", ErrInvalidKey},
		{"#x", "", ErrInvalidKey},
		{"x..x", "", ErrInvalidKey},
	}
	for _, c := range testCase {
		v, err := j.Get(c.key)

		if v.String() != c.val {
			t.Fatalf("get key:%v, result:%v, expect:%v", c.key, v.String(), c.val)
		}

		if c.err != err {
			t.Fatalf("get key:%v, err:%v, expect err:%v", c.key, err, c.err)
		}
		if err != nil {
			t.Logf("get key:%v, err:%v", c.key, err)
		}
	}
}

func TestJSON_SetGet(t *testing.T) {
	//log.SetLevel("DEBUG")
	j := New()
	testCase := []struct {
		path  string
		value Value
	}{
		{"nil", Nil},
		{"struct.nil", Nil},
		{"string", NewString("abc")},
		{"struct.string", NewString("def")},
		{"number_int", NewNumber(1)},
		{"struct.number_int", NewNumber(2)},
		{"number_float", NewNumber(-1.1111)},
		{"struct.number_float", NewNumber(-2.2222)},
		{"bool", NewBool(true)},
		{"struct.bool", NewBool(true)},
		{"array[0]", NewString("a0")},
		{"array[1]", NewString("a1")},
		{"struct.array[0]", NewString("a0")},
		{"struct.array[1]", NewString("a1")},
		{"struct.array2[0].array3[0].array4[0].string", NewString("complicated array")},
	}

	var err error
	for _, v := range testCase {
		//t.Logf("INFO set path:%v, value:%v", v.path, v.value)
		err = j.Set(v.path, v.value)
		if err != nil {
			t.Fatalf("set path:%v, value:%v, error:%v", v.path, v.value, err)
		}

		bs, _ := j.Marshal()
		_ = bs
		//t.Logf("INFO after set: %v", bs)
	}

	for _, v := range testCase {
		value, err := j.Get(v.path)
		//t.Logf("INFO get path:%v, value:%v, err:%v", v.path, value, err)
		if err != nil {
			t.Fatalf("get path:%v, error:%v", v.path, err)
		}

		if value.Type() != v.value.Type() {
			t.Fatalf("get path:%v, expect type:%v, get:%v", v.path, v.value.Type(), value.Type())
		}
		if value.String() != v.value.String() {
			t.Fatalf("get path:%v, expect value:%v, get:%v", v.path, v.value, value)
		}
	}
}

func TestJSON_SetGetInterface(t *testing.T) {
	//log.SetLevel("DEBUG")
	j := New()

	t1 := NewString("def")
	t2 := NewNumber(-2.2222)
	t3, _ := NewJSON([]byte(`{"key":"value"}`))
	t4 := struct {
		Value string `json:"key"`
	}{"value"}

	testCase := []struct {
		pathSet  string
		valueSet interface{}
		pathGet  string
		valueGet interface{}
	}{
		{"basic.uint32", uint32(9999), "basic.uint32", "9999"},
		{"basic.float", float64(-1.111111), "basic.float", "-1.111111"},
		{"basic.bool", true, "basic.bool", "true"},
		{"basic.string", "str", "basic.string", "str"},

		{"struct.nil", Nil, "struct.nil", ""},
		{"struct.string", t1, "struct.string", "def"},
		{"struct.string1", &t1, "struct.string1", "def"},
		{"struct.number_int", t2, "struct.number_int", "-2.2222"},
		{"struct.number_int1", &t2, "struct.number_int1", "-2.2222"},
		{"struct.json", t3, "struct.json.key", "value"},
		{"struct.json1", *t3, "struct.json1.key", "value"},
		{"struct.array[0].json1", t3, "struct.array[0].json1.key", "value"},
		{"struct.array[1].json1", *t3, "struct.array[1].json1.key", "value"},
		{"struct.struct", t4, "struct.struct.key", "value"},
		{"struct.struct1", &t4, "struct.struct1.key", "value"},
	}

	var err error
	for _, v := range testCase {
		//t.Logf("INFO set path:%v, value:%v", v.pathSet, v.valueSet)
		err = j.Set(v.pathSet, v.valueSet)
		if err != nil {
			t.Fatalf("set path:%v, value:%v, error:%v", v.pathGet, v.valueGet, err)
		}

		//t.Logf("INFO after set: %v", j.MarshalString())
	}

	for _, v := range testCase {
		value, err := j.Get(v.pathGet)
		//t.Logf("INFO get path:%v, value:%v, err:%v", v.pathGet, value, err)
		if err != nil {
			t.Fatalf("get path:%v, error:%v", v.pathGet, err)
		}

		if value.String() != fmt.Sprint(v.valueGet) {
			t.Fatalf("get path:%v, expect value:%v, get:%v", v.pathGet, v.valueGet, value.String())
		}
	}
}

func TestJSON_SetArray(t *testing.T) {
	j := New()
	round := 10240
	for i := 0; i < round; i++ {
		path := fmt.Sprintf("path1.path2.path3.array[%v]", i)
		err := j.Set(path, NewNumber(float64(i)))
		if err != nil {
			t.Fatalf("set path:%v, error:%v", path, err)
		}
		////t.Logf("INFO result json:%v", j.MarshalString())

		v, err := j.Get(path)
		if err != nil {
			t.Fatalf("get path:%v, error:%v", path, err)
		}

		nv, err := v.Number()
		if err != nil {
			t.Fatal(err)
		}

		if nv != float64(i) {
			t.Fatalf("get path:%v, result not match, expect:%f, get:%f", path, float64(i), nv)
		}
	}
}

func TestJSON_CalcExpr(t *testing.T) {
	//log.SetLevel("DEBUG")
	src := `
{
  "string": "abcDEF",
  "struct": {
    "string": "hIjK",
    "number_string": "123.456789",
    "number_int": 123,
    "number_float": -123.456789,
    "bool": true,
    "array": [
      "first",
      "second",
      "third"
    ]
  },
  "content": "我叫红领巾，复制【#X00c6t8al3eb8aeff#】",
  "content1": "我叫红领巾"
}
`
	j, err := NewJSON([]byte(src))
	if err != nil {
		t.Fatal(err)
	}
	bs, _ := j.Marshal()
	_ = bs
	//t.Logf("INFO json: %v", bs)

	testCase := []struct {
		expr    string
		resultV string
		resultT VType
	}{
		// test basic get
		{"not_exists", "", TypeNil},
		{"string", "abcDEF", TypeString},
		{"struct.not_exists", "", TypeNil},
		{"struct.bool", "true", TypeBool},
		{"struct.string", "hIjK", TypeString},
		{"struct.number_int", "123", TypeNumber},
		{"struct.number_float", "-123.456789", TypeNumber},

		// test array
		{"struct.array[0]", "first", TypeString},
		{"struct.array[1]", "second", TypeString},
		{"length(struct.array[1])", "6", TypeNumber},
		{"toUpper(struct.array[2])", "THIRD", TypeString},

		// test number functions
		{"toNumber(struct.number_string)", "123.456789", TypeNumber},
		{"toNumber(struct.number_float)", "-123.456789", TypeNumber},
		{"max(struct.number_float, -9999,0,2, 3, 4.01)", "4.01", TypeNumber},
		{"min(struct.number_float, -9999,0,2, 3, 4.01)", "-9999", TypeNumber},
		{"add(struct.number_float, 1.1)", "-122.356789", TypeNumber},
		{"subtract(struct.number_float, 1.1)", "-124.556789", TypeNumber},
		{"multiply(struct.number_float, 10)", "-1234.56789", TypeNumber},
		{"divide(struct.number_int, 123)", "1", TypeNumber},
		{"mod(struct.number_int, 100)", "23", TypeNumber},
		{"sum(struct.number_float, 1.1, -1.1)", "-123.456789", TypeNumber},
		{"average(100, 200, 300)", "200", TypeNumber},

		// test string functions
		{"length(struct.string)", "4", TypeNumber},
		{"toUpper(struct.string)", "HIJK", TypeString},
		{"toLower(struct.string)", "hijk", TypeString},
		{"substr(struct.string, 1)", "IjK", TypeString},
		{"substr(struct.string, 1, 3)", "Ij", TypeString},
		{"substr(struct.string, 1, 10)", "IjK", TypeString},
		{"hasPrefix(struct.string, 'hi')", "false", TypeBool},
		{"hasPrefix(struct.string, 'hI')", "true", TypeBool},
		{"hasSuffix(struct.string, 'jk')", "false", TypeBool},
		{"hasSuffix(struct.string, 'jK')", "true", TypeBool},
		{"contains(struct.string, \"ij\")", "false", TypeBool},
		{"contains(struct.string, \"Ij\")", "true", TypeBool},
		{"append(struct.string, 'abcd', \"ABCD\")", "hIjKabcdABCD", TypeString},
		{"formatString('s=%v&b=%v&f=%v', string, struct.bool, struct.number_float)", "s=abcDEF&b=true&f=-123.456789", TypeString},
		{"formatString('s=%v&b=%t&f=%.f', string, struct.bool, struct.number_int)", "s=abcDEF&b=true&f=123", TypeString},
		{"formatString('s=%v&b=%t&f=%.f', string, struct.bool, int(struct.number_float))", "s=abcDEF&b=true&f=-123", TypeString},
		{"formatString('s=%v&b=%t&f=%v', string, struct.bool, int(struct.number_float))", "s=abcDEF&b=true&f=-123", TypeString},
		{"formatString('xyz')", "xyz", TypeString},
		{"printString('s=%v&b=%t&f=%v', string, struct.bool, int(struct.number_float))", "s=abcDEF&b=true&f=-123", TypeString},

		// test time functions
		// these functions must run in time zone +08:00.
		//{"formatTime(1542766191, 'unix-s', '2006-01-02T15:04:05Z07:00')", "2018-11-21T10:09:51+08:00", TypeString},
		//{"formatLocalTime('2018-11-21 10:09:51', '2006-01-02 15:04:05', '2006-01-02T15:04:05Z07:00')", "2018-11-21T10:09:51+08:00", TypeString},

		// test nested functions
		{"toLower(substr(struct.string, 1))", "ijk", TypeString},
		{"append(struct.string, 'abcd', toUpper(string))", "hIjKabcdABCDEF", TypeString},
		{"toUpper(append(struct.string, 'abcd', toUpper(string)))", "HIJKABCDABCDEF", TypeString},

		// test regexp functions
		{"regexFindAll('#X[a-zA-Z0-9]{8}#|#X[a-zA-Z0-9]{16}#', content)", `["#X00c6t8al3eb8aeff#"]`, TypeJSON},
		{"regexFindAll('#X[a-zA-Z0-9]{8}#|#X[a-zA-Z0-9]{16}#', content1)", `[]`, TypeJSON},
		{"replace(replace(regexFindAll('#X[a-zA-Z0-9]{8}#|#X[a-zA-Z0-9]{16}#', content), '#X', ''), '#', '')", `["00c6t8al3eb8aeff"]`, TypeString},
		{"replace(replace(regexFindAll('#X[a-zA-Z0-9]{8}#|#X[a-zA-Z0-9]{16}#', content1), '#X', ''), '#', '')", `[]`, TypeString},
		{"decodeJSON(replace(replace(regexFindAll('#X[a-zA-Z0-9]{8}#|#X[a-zA-Z0-9]{16}#', content1), '#X', ''), '#', ''))", `[]`, TypeJSON},
		{"decodeJSON(replace(replace(regexFindAll('#X[a-zA-Z0-9]{8}#|#X[a-zA-Z0-9]{16}#', content), '#X', ''), '#', ''))", `["00c6t8al3eb8aeff"]`, TypeJSON},
	}

	//testCase = testCase[len(testCase)-1:]

	for _, v := range testCase {
		rst, err := j.Calculate(v.expr)
		if err != nil {
			t.Fatalf("expr:%v, err:%v", v.expr, err)
		}
		//t.Logf("INFO: expr:%v, result value:%v, result type:%v", v.expr, rst.String(), rst.Type())
		if rst.Type() != v.resultT {
			t.Fatalf("expr:%v, expect result type:%v, get:%v", v.expr, v.resultT.String(), rst.Type())
		}
		if rst.String() != v.resultV {
			t.Fatalf("expr:%v, expect result value:%v, get:%v", v.expr, v.resultV, rst.String())
		}
	}
}

func TestSetJSON(t *testing.T) {
	src := `
{
  "struct": {
     "string": "abcd"
  }
}`

	j, err := NewJSON([]byte(src))
	if err != nil {
		t.Fatal(err)
	}

	subj, err := NewJSON([]byte(src))
	if err != nil {
		t.Fatal(err)
	}

	err = j.Set("struct.sub", subj)
	if err != nil {
		t.Fatal(err)
	}

	//t.Log(j.MarshalString())
	s, _ := j.Get("struct.sub.struct.string")
	if s.String() != "abcd" {
		t.Fatalf("expected:abcd, got:%v", s.String())
	}
}

func TestJSON_Del(t *testing.T) {
	m := map[string]interface{}{
		"string": "abc",
		"number": 1.1,
		"bool":   true,
		"array":  []int{1, 2, 3},
		"map": map[string]string{
			"abc": "abc",
			"def": "def",
		},
		"struct": struct {
			S string `json:"s"`
			I int    `json:"i"`
		}{S: "str", I: 1},
	}

	j, err := NewJSONFromInterface(map[string]interface{}{"struct": m})
	if err != nil {
		t.Fatal(err)
	}

	testCase := []struct {
		toDelete string
		toGet    string
		expected interface{}
	}{
		{"struct.string", "struct.string", ""},
		{"struct.array[1]", "struct.array", []int{1, 3}},
		{"struct.map.abc", "struct.map", map[string]string{"def": "def"}},
		{"struct.struct.s", "struct.struct", map[string]int{"i": 1}},
		{"struct.struct.NONE", "struct.struct", map[string]int{"i": 1}},
		{"NONE", "struct.struct", map[string]int{"i": 1}},
	}

	for _, v := range testCase {
		err = j.Del(v.toDelete)
		if err != nil {
			t.Fatalf("delete path:%v, err:%v, json:%v", v.toDelete, err, j.MarshalString())
		}
		tmp, _ := j.Get(v.toGet)
		//t.Logf("get %v:%v", v.toGet, tmp.String())
		exp, _ := NewValue(v.expected)
		if tmp.String() != exp.String() {
			t.Fatalf("path:%v, expected:%v, get:%v", v.toGet, exp.String(), tmp.String())
		}
	}
}

func TestInvalidPath(t *testing.T) {
	var err error
	j := New()

	invalidCase := []string{
		"",
		"1",
		"2xx",
		"xx^",
		"xx,",
		"xx)",
		"xx..xx",
		"xx.1.xx",
		"xx.2xx.xx",
		"xx.xx().xx",
	}

	for _, k := range invalidCase {
		err = j.Set(k, nil)
		if err == nil {
			t.Fatalf("set key:%v, expected to return an error", k)
		}
		//t.Logf("set key:%v, err:%v", k, err)

		_, err = j.Get(k)
		if err == nil {
			t.Fatalf("get key:%v, expected to return an error", k)
		}
		//t.Logf("get key:%v, err:%v", k, err)
	}

	invalidCalcCase := []string{
		"",
		"2xx",
		"xx^",
		"xx,",
		"xx)",
		"xx..xx",
		"xx.2xx.xx",
		"xx.xx(.xx",
		"xx.xx().xx",
	}
	for _, k := range invalidCalcCase {
		_, err = j.Calculate(k)
		if err == nil {
			t.Fatalf("calculate key:%v, expected to return an error", k)
		}
		//t.Logf("calculate key:%v, err:%v", k, err)
	}
}

func BenchmarkJSONSet(b *testing.B) {
	j := New()
	var err error
	for i := 0; i < b.N; i++ {
		if err = j.Set("key", "val"); err != nil {
			b.Fatal("error:", err)
		}
	}
}

func BenchmarkMapSet(b *testing.B) {
	m := make(map[string]string)
	for i := 0; i < b.N; i++ {
		m["key"] = "val"
	}
}

func BenchmarkJSONGet(b *testing.B) {
	j := New()
	j.Set("key", "val")
	var val Value
	for i := 0; i < b.N; i++ {
		val, _ = j.Get("key")
		if val.String() != "val" {
			b.Fatal("result not match")
		}
	}
}

func BenchmarkMapGet(b *testing.B) {
	m := make(map[string]string)
	m["key"] = "val"
	for i := 0; i < b.N; i++ {
		if m["key"] != "val" {
			b.Fatal("result not match")
		}
	}
}

/* not supported yet
func TestJSON_DelArray(t *testing.T) {
	m := [][]int{
		{1, 2, 3},
		{4, 5, 6},
		{7, 8, 9},
	}
	j, err := NewJSONFromInterface(map[string]interface{}{"arrays": m})
	if err != nil {
		t.Fatal(err)
	}
	t.Log(j.MarshalString())
}
*/

func BenchmarkNewJSON(b *testing.B) {
	src := []byte(`
{
  "dep1.dep2.dep3.a": "a",
  "dep1.dep2.dep3": {
    "b": "b"
  },
  "dep1.dep2": {
    "dep3.c": "c",
    "dep3": {
      "d": "d"
    }
  },
  "dep1": {
    "dep2.dep3.e": "e",
    "dep2.dep3": {
      "f": "f"
    },
    "dep2": {
      "dep3": {
        "g": "g"
      }
    }
  }
}`)
	CheckKeySyntax(true)
	var err error
	for i := 0; i < b.N; i++ {
		if _, err = NewJSON(src); err != nil {
			b.Fatal("error:", err)
		}
	}
}

func TestJSON_MapRange(t *testing.T) {
	j := New()
	j.Set("k1", "v1")
	j.Set("k2", "v2")
	j.Set("k3", "v3")
	j.Set("k4", "v4")

	j.MapRange(func(key string, value interface{}) bool {
		t.Logf("key:%v, value:%v", key, value)
		return value != "v3"
	})
}

func TestJSON_ArrayRange(t *testing.T) {
	j, err := NewJSON([]byte(`["v1", "v2", "v3", "v4"]`))
	if err != nil {
		t.Fatal(err)
	}

	j.ArrayRange(func(index int, value interface{}) bool {
		t.Logf("index:%v, value:%v", index, value)
		return value != "v3"
	})
}
