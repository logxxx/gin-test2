package jsonutil

import (
	"os"
	"strconv"
	"sync"
	"sync/atomic"
	"time"
)

var (
	exprCacheSize        int32 // 表达式编译缓存大小，小于等于0：不缓存
	exprCacheCurrentSize int32
	exprCache            = sync.Map{}
	once                 = sync.Once{}
)

func init() {
	n, _ := strconv.Atoi(os.Getenv("JSON_UTIL_CACHE_SIZE"))
	exprCacheSize = int32(n)
	cleanExprCacheBackground()
}

// SetExprCache set the size of expression compiling cache.
// MUST be set when program start! And at most set once!
func SetExprCache(size int32) {
	once.Do(func() {
		if size >= 0 {
			exprCacheSize = size
		}
	})
}

// CleanExprCache clean compiling cache.
func CleanExprCache() {
	exprCache.Range(func(key, value interface{}) bool {
		exprCache.Delete(key)
		return true
	})
}

func cleanExprCacheBackground() {
	go func() {
		for {
			time.Sleep(time.Second * 1)
			if exprCacheSize <= 0 {
				return
			}
			if n := atomic.LoadInt32(&exprCacheCurrentSize); n >= exprCacheSize {
				// 每次清空一半缓存
				var i int32
				x := (n + 1) / 2
				exprCache.Range(func(key, value interface{}) bool {
					if i < x {
						exprCache.Delete(key)
						i++
						return true
					}
					return false
				})
				atomic.AddInt32(&exprCacheCurrentSize, -1*i)
			}
		}
	}()
}
