package jsonutil

import (
	"errors"
	"fmt"
	"logxxx.com/test/v2/utils/log"
	"reflect"
	"strconv"
	"strings"
)

// main errors
var (
	ErrTypeAssertion = errors.New("type assertion failed")
	ErrInvalidKey    = errors.New("invalid key")
	ErrNilJSON       = errors.New("nil json")
)

const (
	strTrue  = "true"
	strFalse = "false"

	prefixConst = "$CST#"
	prefixFunc  = "$FUNC#"
	delimiter   = byte('.')
)

var (
	checkKeySyntax = true
	typeJSON       = reflect.TypeOf(JSON{})
	typeValue      = reflect.TypeOf(Value{})
)

type (
	// Array is a slice of interface{}
	Array = []interface{}
	// Map is a map with string key and interface{} value
	Map = map[string]interface{}
)

// JSON define a struct stored a json object.
type JSON struct {
	isKeyPureString bool
	data            interface{}
}

// CheckKeySyntax specify whether to check syntax of key in NewJSON/Get/Set, default is true.
func CheckKeySyntax(b bool) {
	checkKeySyntax = b
}

// New return an empty JSON struct.
func New() *JSON {
	return &JSON{data: make(Map)}
}

func NewWithPureKey() *JSON {
	return &JSON{data: make(Map), isKeyPureString: true}
}

// NewJSON return a JSON unmarshal from data.
func NewJSONWithPureKey(data []byte) (*JSON, error) {
	j := NewWithPureKey()
	err := interpreter.Unmarshal(data, &j.data)
	if err != nil {
		return nil, err
	}

	if checkKeySyntax {
		if err := j.checkValue(); err != nil {
			return nil, err
		}
	}
	return j, nil
}

// NewJSON return a JSON unmarshal from data.
func NewJSON(data []byte) (*JSON, error) {
	j := New()
	err := interpreter.Unmarshal(data, &j.data)
	if err != nil {
		return nil, err
	}

	if checkKeySyntax {
		if err := j.checkValue(); err != nil {
			return nil, err
		}
	}
	return j, nil
}

func (j *JSON) checkValue() error {
	switch j.data.(type) {
	case Map:
		m := j.data.(Map)
		specialKV, err := removeKeysWithDelimiter(m)
		if err != nil {
			return err
		}

		for k, v := range specialKV {
			if err := j.set(k, v); err != nil {
				return fmt.Errorf("set key:%v, error:%v", k, err)
			}
		}

		for _, v := range m {
			sub := &JSON{data: v}
			if err := sub.checkValue(); err != nil {
				return err
			}
		}

	case Array:
		a := j.data.(Array)
		for _, v := range a {
			sub := &JSON{data: v}
			if err := sub.checkValue(); err != nil {
				return err
			}
		}
	}
	return nil
}

func removeKeysWithDelimiter(m Map) (Map, error) {
	if m == nil {
		return nil, nil
	}
	specialKV := make(Map)
	for k, v := range m {
		pos := strings.IndexByte(k, delimiter)
		if pos < 0 {
			continue
		}
		if pos == 0 || pos == len(k)-1 {
			return nil, ErrInvalidKey
		}

		parseToBasicKV(k, v, specialKV)
		delete(m, k)
	}
	return specialKV, nil
}

func parseToBasicKV(k string, v interface{}, kv Map) {
	if m, ok := v.(Map); ok {
		for kk, vv := range m {
			parseToBasicKV(k+string(delimiter)+kk, vv, kv)
		}
		return
	}
	kv[k] = v
}

// NewJSONFromInterface return a JSON unmarshal from bs.
func NewJSONFromInterface(data interface{}) (*JSON, error) {
	bs, err := interpreter.Marshal(data)
	if err != nil {
		return nil, err
	}
	return NewJSON(bs)
}

// Interface return the data of JSON.
func (j *JSON) Interface() interface{} {
	return j.data
}

// Map type asserts to map[string]interface{}
func (j *JSON) Map() (Map, error) {
	if j == nil {
		return nil, ErrNilJSON
	}
	mp, ok := j.data.(Map)
	if !ok {
		return nil, ErrTypeAssertion
	}
	return mp, nil
}

// MapRange if j is a map, calls f sequentially for each key and value present in the map.
// If f returns false, range stops the iteration.
func (j *JSON) MapRange(f func(key string, value interface{}) bool) {
	if j == nil || j.data == nil {
		return
	}
	mp, ok := j.data.(Map)
	if !ok {
		return
	}
	for k, v := range mp {
		if !f(k, v) {
			break
		}
	}
}

// Array type asserts to []interface{}
func (j *JSON) Array() (Array, error) {
	if j == nil {
		return nil, ErrNilJSON
	}
	a, ok := j.data.(Array)
	if !ok {
		return nil, ErrTypeAssertion
	}
	return a, nil
}

// ArrayRange if j is a array, calls f sequentially for each index and value present in the array.
// If f returns false, range stops the iteration.
func (j *JSON) ArrayRange(f func(index int, value interface{}) bool) {
	if j == nil || j.data == nil {
		return
	}
	a, ok := j.data.(Array)
	if !ok {
		return
	}
	for i, v := range a {
		if !f(i, v) {
			break
		}
	}
}

// Get return the value of path.
// If the result is a json array or map, marshal it to string.
func (j *JSON) Get(path string) (Value, error) {
	if j == nil || j.data == nil {
		return Nil, ErrNilJSON
	}

	val, err := j.get(path)
	if err != nil {
		return Nil, err
	}
	return NewValue(val)
}

// MustGet return the value of path, ignore error.
// If the result is a json array or map, marshal it to string.
func (j *JSON) MustGet(path string) Value {
	v, _ := j.Get(path)
	return v
}

// GetJSON return the sub JSON of path.
func (j *JSON) GetJSON(path string) (*JSON, error) {
	if j == nil || j.data == nil {
		return nil, ErrNilJSON
	}
	val, err := j.get(path)
	if err != nil {
		return nil, err
	}
	return &JSON{data: val}, nil
}

// GetInterface return the value as interface of path.
func (j *JSON) GetInterface(path string) (interface{}, error) {
	if j == nil || j.data == nil {
		return nil, ErrNilJSON
	}
	return j.get(path)
}

func (j *JSON) get(path string) (interface{}, error) {
	if path == "" {
		return nil, errors.New("path is empty")
	}

	var err error
	data := j.data
	begin := 0
	for i := 0; i < len(path); i++ {
		if i+1 >= len(path) {
			data, err = getKey(data, path[begin:])
			if err != nil {
				return nil, err
			}
			break
		}
		if path[i] == delimiter {
			data, err = getKey(data, path[begin:i])
			if err != nil {
				return nil, err
			}
			begin = i + 1
		}
	}
	return data, nil
}

func getKey(tmp interface{}, key string) (interface{}, error) {
	if !isValidKey(key) {
		return nil, ErrInvalidKey
	}

	// 3ns
	index := -1
	var err error
	key, index, err = parseKey(key)
	if err != nil {
		log.Errorf("getKey parseKey err:%v p1:%+v", err, key)
		return nil, err
	}

	if tmp == nil {
		return nil, nil // key not exists
	}

	m, ok := tmp.(Map)
	if !ok {
		return nil, ErrTypeAssertion
	}

	tmp = m[key] // 6ns
	if tmp == nil {
		return nil, nil
	}

	if index >= 0 {
		// get value of array by index
		a, ok := tmp.(Array)
		if !ok {
			return nil, ErrTypeAssertion
		}
		if index >= len(a) {
			return nil, nil
		}
		tmp = a[index]
	}
	//log.Debugf("get path:%v, value:%v", key, tmp)
	return tmp, nil
}

// Deleted on 2018.11.02
// SetInterface set the value at path to value.
//func (j *JSON) SetInterface(path string, value interface{}) error {
//	return j.Set(path, value)
//}

// Set set the value at path to value.
func (j *JSON) Set(path string, value interface{}) error {
	if j == nil || j.data == nil {
		return ErrNilJSON
	}

	rv := reflect.Indirect(reflect.ValueOf(value))
	k := rv.Kind()
	switch k {
	case reflect.String,
		reflect.Bool,
		reflect.Int,
		reflect.Int8,
		reflect.Int16,
		reflect.Int32,
		reflect.Int64,
		reflect.Uint,
		reflect.Uint8,
		reflect.Uint16,
		reflect.Uint32,
		reflect.Uint64,
		reflect.Float32,
		reflect.Float64:
		return j.set(path, rv.Interface())
	default:
		if k == reflect.Struct {
			t := rv.Type()
			if t == typeValue {
				v := rv.Interface().(Value)
				return j.setValue(path, &v)
			}
			if t == typeJSON {
				v := rv.Interface().(JSON)
				return j.set(path, v.data)
			}

		}
		v, err := NewJSONFromInterface(value)
		if err != nil {
			return err
		}
		return j.set(path, v.data)
	}
}

func (j *JSON) setValue(path string, value *Value) error {
	var v interface{}
	var err error
	switch value.Type() {
	case TypeNumber:
		v, err = value.Number()
		if err != nil {
			return err
		}
	case TypeString:
		v = value.String()
	case TypeBool:
		v, err = value.Bool()
		if err != nil {
			return err
		}

	case TypeJSON:
		jv, err := value.JSON()
		if err != nil {
			return err
		}

		v = jv.data
	default:
		v = nil
	}
	return j.set(path, v)
}

func (j *JSON) set(path string, value interface{}, wantToDelete ...bool) error {
	if path == "" {
		return errors.New("path is empty")
	}

	isDelete := len(wantToDelete) == 1 && wantToDelete[0]

	p := make([]string, 0)
	if j.isKeyPureString {
		p = strings.Split(path, string(delimiter))
		for _, v := range p {
			if !isValidKey(v) {
				err := ErrInvalidKey
				log.Errorf("set isValidKey err:%v p1:%+v p2:%+v p3:%+v", err, path, p, v)
				return err
			}
		}
	} else {
		p = []string{path}
	}

	depth := len(p)
	i := 0
	parent := j.data
	var isArray bool
	indexToSet := -1
	keyToSet := p[0]

	for ; i < depth; i++ {
		key := p[i]
		m, ok := parent.(Map)
		if !ok {
			return ErrTypeAssertion
		}

		key, index, err := parseKey(key)
		if err != nil {
			log.Errorf("set parseKey err:%v p1:+%v", err, key)
			return err
		}

		if i == depth-1 && index < 0 {
			if isDelete {
				delete(m, key)
				return nil
			}
			keyToSet = key
			break
		}

		cur := m[key]
		if cur == nil {
			if index > 0 {
				return errors.New("index out of range")
			} else if index == 0 {
				m[key] = make(Array, 1)
				parent = m[key]
				isArray = true
				indexToSet = 0
			} else {
				keyToSet = key
			}
			break
		}

		if index >= 0 {
			// get value of array by index
			a, ok := cur.(Array)
			if !ok {
				return ErrTypeAssertion
			}
			size := len(a)

			if isDelete {
				if index >= size {
					return errors.New("index out of range")
				}
				if i == depth-1 {
					isArray = true
					indexToSet = index
					for i := index; i < size-1; i++ {
						a[i] = a[i+1]
					}
					a = a[:size-1]
					m[key] = a
					return nil
				}
			} else {
				if index > len(a) {
					return errors.New("index out of range")
				}
				if index == len(a) {
					isArray = true
					indexToSet = index
					m[key] = append(a, 0) // append is dangerous
					parent = m[key]
					break
				}
				if i == depth-1 {
					isArray = true
					indexToSet = index
					parent = cur
					break
				}
			}

			cur = a[index]
			if cur == nil {
				isArray = true
				indexToSet = index
				parent = cur
				break
			}
		}
		parent = cur
	}

	data := value
	for j := len(p) - 1; j > i; j-- {
		key, index, err := parseKey(p[j])
		if err != nil {
			return err
		}
		if index > 0 {
			return errors.New("index out of range")
		} else if index == 0 {
			data = Map{key: Array{data}}
		} else {
			data = Map{key: data}
		}
	}

	if isDelete {
		return nil // path not exists
	}

	// not delete
	if isArray {
		parent.(Array)[indexToSet] = data
		return nil
	}

	if data == Nil {
		data = nil
	}
	parent.(Map)[keyToSet] = data
	return nil
}

// Del delete the value at path. The end of path must be map or slice.
func (j *JSON) Del(path string) error {
	if j == nil || j.data == nil {
		return ErrNilJSON
	}
	return j.set(path, nil, true)
}

// Calculate calculate the value of expression.
// expression can contains functions, support nesting functions.
func (j *JSON) Calculate(expr string) (ret Value, err error) {
	if j == nil {
		return Nil, ErrNilJSON
	}

	defer func() {
		if err := recover(); err != nil {
			log.Errorf("recover error:", err)
			ret = Nil
			err = fmt.Errorf("%v", err)
		}
		//log.Debug("expr:", expr, " result:", ret)
	}()

	ex, err := CompileExpr(expr)
	if err != nil {
		return Nil, err
	}

	return j.getWithSyntax(ex.expr, ex)
}

// Marshal marshal JSON to bytes
func (j *JSON) Marshal() ([]byte, error) {
	if j == nil || j.data == nil {
		return nil, ErrNilJSON
	}
	return interpreter.Marshal(j.data)
}

//Unmarshal unmarshal JSON to interface
func (j *JSON) Unmarshal(v interface{}) error {
	data, err := j.Marshal()
	if err != nil {
		return err
	}

	return interpreter.Unmarshal(data, v)
}

// MarshalString marshal JSON to string, ignore error
func (j *JSON) MarshalString() string {
	var v interface{}
	if j != nil {
		v = j.data
	}
	bs, _ := interpreter.Marshal(v)
	return string(bs)
}

func (j *JSON) doFunction(f *function) (Value, error) {
	ft, ok := functions[f.name]
	if !ok {
		return Nil, errors.New("unknown function: " + f.name)
	}
	finalArgs := make([]Value, 0, len(f.args))
	for i := 0; i < len(f.args); i++ {
		v, err := j.getWithSyntax(f.args[i], f.rootExpr)
		if err != nil {
			return Nil, err
		}
		finalArgs = append(finalArgs, v)
	}
	v, err := ft(j, finalArgs)
	//log.Debugf("function:%v, args:%v, result:%v, error:%v", funcName, argsExpr, v, err)
	return v, err
}

func (j *JSON) getWithSyntax(key string, rootExpr *Expression) (Value, error) {
	key = strings.TrimSpace(key)
	if key == "" {
		return Nil, errors.New("empty key")
	}

	if strings.HasPrefix(key, prefixConst) {
		return rootExpr.constantMap[key], nil
	}
	if strings.HasPrefix(key, prefixFunc) {
		return j.doFunction(rootExpr.functionMap[key])
	}

	switch key {
	case strFalse:
		return NewBool(false), nil
	case strTrue:
		return NewBool(true), nil
	}

	if (key[0] >= '0' && key[0] <= '9') || key[0] == '-' {
		f, err := strconv.ParseFloat(key, 64)
		if err != nil {
			return Nil, err
		}
		return NewNumber(f), nil
	}

	return j.Get(key)
}

func parseKey(s string) (key string, arrayIndex int, err error) {
	arrayIndex = -1
	l := len(s)
	if l == 0 {
		err = errors.New("empty key")
		return
	}

	if s[l-1] == ']' {
		begin := strings.IndexByte(s, '[')
		if begin < 0 {
			err = ErrInvalidKey
			return
		}
		idx, e := strconv.Atoi(s[begin+1 : l-1])
		if e != nil || idx < 0 {
			log.Errorf("parseKey strconv.Atoi err:%v p1:%+v", e, s)
			err = ErrInvalidKey
			return
		}

		key = s[:begin]
		arrayIndex = idx
		return
	}
	key = s
	return
}

func parseInt(s string, dft ...int64) (int64, error) {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		if len(dft) > 0 {
			return dft[0], nil
		}
		return 0, err
	}
	return int64(f), nil
}

func parseFloat(s string, dft ...float64) (float64, error) {
	f, err := strconv.ParseFloat(s, 64)
	if err != nil {
		if len(dft) > 0 {
			return dft[0], nil
		}
		return 0, err
	}
	return f, nil
}

func isValidKey(name string) bool {
	if name == "" {
		return false
	}

	if !isValidCharForFirstKey(name[0]) {
		return false
	}

	if !checkKeySyntax {
		return true
	}

	for _, b := range []byte(name[1:]) {
		if !isValidCharForKey(b) {
			return false
		}
	}
	return true
}

func isValidCharForFirstKey(b byte) bool {
	if b >= 'a' && b <= 'z' {
		return true
	}
	if b >= 'A' && b <= 'Z' {
		return true
	}
	switch b {
	case '-', '_', '[', ']', '@', '/', '.':
		return true
	}
	return false
}

func isValidCharForKey(b byte) bool {
	if b >= 'a' && b <= 'z' {
		return true
	}
	if b >= 'A' && b <= 'Z' {
		return true
	}
	if b >= '0' && b <= '9' {
		return true
	}
	switch b {
	case '-', '_', '[', ']', '@', '/':
		return true
	}
	return false
}
