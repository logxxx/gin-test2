package jsonutil

import (
	"fmt"
	"testing"
	"time"
)

func TestLengthFunction(t *testing.T) {
	repBuff := []byte(`
{
	"myarry": [{
			"a1": "aaa"
		},
		{
			"b1": "bbb"
		},
		{
			"c1": "ccc"
		}
	],
	"mymap": {
		"a": "a1",
		"b": "b1"
	},
	"myint": 1,
	"mystr": "abc",
	"myhz": "hello,我是中国人"
}
`)
	t.Log(string(repBuff))
	judge, err := NewJSON(repBuff)
	if err != nil {
		t.Log("解析body到json失败", err.Error())
		t.Error(err.Error())
	}
	jval, err := judge.Calculate("length(myarry)")
	if err != nil {
		t.Log("解析失败:", err.Error())
		t.Error(err.Error())
	}
	t.Log("length(myarry):", jval)
	jval, err = judge.Calculate("length(mymap)")
	if err != nil {
		t.Log("解析失败:", err.Error())
		t.Error(err.Error())
	}
	t.Log("length(mymap):", jval)
	jval, err = judge.Calculate("length(myint)")
	if err != nil {
		fmt.Println("解析失败:", err.Error())
		t.Error(err.Error())
	}
	t.Log("length(myint):", jval)
	jval, err = judge.Calculate("length(mystr)")
	if err != nil {
		t.Error("解析失败:", err.Error())
	}
	t.Log("length(mystr):", jval)

	jval, err = judge.Calculate("runelength(myhz)")
	if err != nil {
		t.Error("解析失败:", err.Error())
	}
	t.Log("runelength(myhz):", jval)

}

func TestRegisterFunction(t *testing.T) {
	var err error
	_ = time.Now()

	err = RegisterFunction("2xx", func(input *JSON, args []Value) (Value, error) {
		return Nil, nil
	})
	if err == nil {
		t.Fatal("register function start with number success")
	}
	//t.Log("INFO:", err)

	j := New()
	f := float64(2.5)
	j.Set("struct.number", NewNumber(f))
	j.Set("struct.string", "hello")
	err = RegisterFunction("pow", func(input *JSON, args []Value) (Value, error) {
		if len(args) != 1 {
			return Nil, ErrArgsSize
		}
		f, err := args[0].Number()
		if err != nil {
			return Nil, ErrArgsType
		}
		return NewNumber(f * f), nil
	})

	if err != nil {
		t.Fatal(err)
	}

	err = RegisterFunction("hello", func(input *JSON, args []Value) (Value, error) {
		var name string
		for i, arg := range args {
			if i != 0 {
				name += ", "
			}
			name += arg.String()
		}
		hello, _ := input.Get("struct.string")
		return NewString(hello.String() + " " + name), nil
	})

	hello, err := j.Calculate("hello('lnk', 'rick')")
	if hello.String() != "hello lnk, rick" {
		t.Fatalf("hello result not match, expect: %v, get: %v", "hello lnk, rick", hello.String())
	}

	v, err := j.Calculate("pow(struct.number)")
	if err != nil {
		t.Fatal(err)
	}

	nv, err := v.Number()
	if err != nil {
		t.Fatal(err)
	}

	if nv != f*f {
		t.Fatalf("pow result not match, expect:%f, get:%f", f*f, nv)
	}

	err = RegisterFunction("pow", func(input *JSON, args []Value) (Value, error) {
		if len(args) != 1 {
			return Nil, ErrArgsSize
		}
		f, err := args[0].Number()
		if err != nil {
			return Nil, ErrArgsType
		}
		return NewNumber(f * f * f), nil
	}, true)

	if err != nil {
		t.Fatal(err)
	}

	v, err = j.Calculate("pow(struct.number)")
	if err != nil {
		t.Fatal(err)
	}

	nv, err = v.Number()
	if err != nil {
		t.Fatal(err)
	}

	if nv != f*f*f {
		t.Fatalf("pow result not match, expect:%f, get:%f", f*f*f, nv)
	}
}

func TestRegexMatch(t *testing.T) {
	testCase := []struct {
		regex Value
		str   Value
		match bool
	}{
		{NewString("^[0-9]+$"), NewString("123456"), true},
		{NewString("^[0-9]+$"), NewString("123456$"), false},
		{NewString("^[0-9a-zA-Z]+$"), NewString("123456abcd"), true},
		{NewString("^[0-9a-zA-Z]+$"), NewString("#123456abcd"), false},
	}

	for _, c := range testCase {
		v, err := funcRegexMatch(New(), []Value{c.regex, c.str})
		if err != nil {
			t.Fatal(err)
		}
		//t.Logf("test case:%+v, get result:%v", c, v)
		bv, err := v.Bool()
		if err != nil {
			t.Fatal(err)
		}

		if bv != c.match {
			t.Fatalf("test case:%+v, get result:%v", c, v)
		}
	}
}

func TestRegexFind(t *testing.T) {
	testCase := []struct {
		regex Value
		str   Value
		find  []string
	}{
		{NewString("2.*5"), NewString("123456"), []string{"2345"}},
		{NewString("[0-9]+"), NewString("123456$"), []string{}},
		{NewString("[0-9]+?"), NewString("123456$"), []string{}},
		{NewString("[a-zA-Z]+$"), NewString("#123456abcd"), []string{}},
		{NewString("[a-z]{2}"), NewString("#123456abcde"), []string{}},
		{NewString("[a-z]{2}$"), NewString("#123456abcde"), []string{}},
	}

	for _, c := range testCase {
		v, err := funcRegexFindAll(New(), []Value{c.regex, c.str})
		if err != nil {
			t.Fatal(err)
		}
		t.Logf("test case:%+v, get result:%v", c, v)
	}
}

func TestRegexReplace(t *testing.T) {
	testCase := []struct {
		regex   Value
		src     Value
		replace Value
		dst     Value
	}{
		{NewString("[0-9]"), NewString("a123b"), NewString("X"), NewString("aXXXb")},
		{NewString("[0-9]+"), NewString("a123b"), NewString("X"), NewString("aXb")},
		{NewString("[a-z]"), NewString("a123b"), NewString("X"), NewString("X123X")},
		{NewString("[a-z]+"), NewString("a123b"), NewString("X"), NewString("X123X")},
		{NewString("123"), NewString("a123b"), NewString("X"), NewString("aXb")},
	}

	for _, c := range testCase {
		v, err := funcRegexReplaceAll(New(), []Value{c.regex, c.src, c.replace})
		if err != nil {
			t.Fatal(err)
		}
		t.Logf("test case:%+v, get result:%v", c, v)

		if v.String() != c.dst.String() {
			t.Fatalf("result not match, case:%v, result:%v", c, v)
		}
	}
}

func TestReplace(t *testing.T) {
	ary, _ := NewValue([]string{"a123b", "A123B"})
	mp, _ := NewValue(map[string]int{
		"a123b": 123,
		"A123B": 123,
	})
	testCase := []struct {
		src Value
		old Value
		new Value
		dst Value
	}{
		{NewString("a123b"), NewString("0"), NewString("X"), NewString("a123b")},
		{NewString("a123a"), NewString("a"), NewString(""), NewString("123")},
		{NewString("a123b2"), NewString("2"), NewString("X"), NewString("a1X3bX")},
		{NewNumber(123), NewString("2"), NewString("9"), NewString("193")},
		{mp, NewString("2"), NewString("9"), NewString(`{"A193B":193,"a193b":193}`)},
		{ary, NewString("2"), NewString("9"), NewString(`["a193b","A193B"]`)},
	}

	for _, c := range testCase {
		v, err := funcReplace(New(), []Value{c.src, c.old, c.new})
		if err != nil {
			t.Fatal(err)
		}
		t.Logf("test case:%+v, get result:%v", c, v)

		if v.String() != c.dst.String() {
			t.Fatalf("result not match, case:%v, result:%v", c, v)
		}
	}
}

func TestMarshal(t *testing.T) {
	type user struct {
		Name string `json:"name"`
		Age  int    `json:"age"`
	}

	var u = user{
		Name: "lnk",
		Age:  28,
	}

	j, err := NewJSONFromInterface(&u)
	if err != nil {
		t.Fatal(err)
	}

	var nu user
	j.Set("name", "xzj")
	j.Unmarshal(&nu)
	if u.Name == nu.Name {
		t.Fatalf("result not mach, case %v, result: %v", u.Name, nu.Name)
	}
}

/*
func TestHTTP(t *testing.T) {
	// GET
	testCase := []string{
		"http://xxxx",
		"http://yyy",
	}

	for _, v := range testCase {
		j := New()
		j.Set("url", v)
		v, _ := j.Calculate("httpGet(url)")
		_ = v
		//t.Log("HTTP GET result:", v.String())
	}

	// POST
	testCase2 := []string{
		"http://risk.reg:6392/shield/v1.1/check?event=test",
	}

	for _, v := range testCase2 {
		j := New()
		j.Set("url", v)
		v, _ := j.Calculate(`httpPost(url, '', '{\"evnet\":\"test\"}')`)
		_ = v
		//t.Log("HTTP POST result:", v.String())
	}
}
*/

/* this function must run in time zone +08:00.
func TestFormatTime(t *testing.T) {
	testCase := []struct {
		value, srcFormat, dstFormat, expect string
	}{
		{"now", "", "", time.Now().Format(time.RFC3339)},
		{"now", "xxx", "2006-01-02 15:04:05", time.Now().Format("2006-01-02 15:04:05")},
		{"1542719698573679000", "unix-ns", "unix-ns", "1542719698573679000"},
		{"1542719698573679", "unix-us", "unix-us", "1542719698573679"},
		{"1542719698573", "unix-ms", "unix-ms", "1542719698573"},
		{"1542719698", "unix-s", "unix-s", "1542719698"},
		{"2018-11-20 21:45:29+08:00", "2006-01-02 15:04:05Z07:00", "unix-ns", "1542721529000000000"},
		{"2018-11-20 21:45:29+08:00", "2006-01-02 15:04:05Z07:00", "unix-us", "1542721529000000"},
		{"2018-11-20 21:45:29+08:00", "2006-01-02 15:04:05Z07:00", "unix-ms", "1542721529000"},
		{"2018-11-20 21:45:29+08:00", "2006-01-02 15:04:05Z07:00", "unix-s", "1542721529"},
		{"2018-11-20 21:45:29+08:00", "2006-01-02 15:04:05Z07:00", "2006-01-02 15:04:05Z07:00", "2018-11-20 21:45:29+08:00"},
	}

	for _, v := range testCase {
		r, err := funcFormatTime([]string{v.value, v.srcFormat, v.dstFormat})
		_ = err
		//t.Logf("INFO: case:%+v, result:%v, err:%v", v, r, err)
		if v.expect != r.String() {
			t.Fatalf("expect:%v, get:%v", v.expect, r.String())
		}
	}
}

func TestFormatLocalTime(t *testing.T) {
	testCase := []struct {
		value, srcFormat, dstFormat, expect string
	}{
		{"now", "", "", time.Now().Format(time.RFC3339)},
		{"now", "xxx", "2006-01-02 15:04:05", time.Now().Format("2006-01-02 15:04:05")},
		{"1542719698573679000", "unix-ns", "unix-ns", "1542719698573679000"},
		{"1542719698573679", "unix-us", "unix-us", "1542719698573679"},
		{"1542719698573", "unix-ms", "unix-ms", "1542719698573"},
		{"1542719698", "unix-s", "unix-s", "1542719698"},
		{"2018-11-20 21:45:29", "2006-01-02 15:04:05", "unix-ns", "1542721529000000000"},
		{"2018-11-20 21:45:29", "2006-01-02 15:04:05", "unix-us", "1542721529000000"},
		{"2018-11-20 21:45:29", "2006-01-02 15:04:05", "unix-ms", "1542721529000"},
		{"2018-11-20 21:45:29", "2006-01-02 15:04:05", "unix-s", "1542721529"},
		{"2018-11-20 21:45:29", "2006-01-02 15:04:05", "2006-01-02 15:04:05", "2018-11-20 21:45:29"},
	}

	for _, v := range testCase {
		r, err := funcFormatLocalTime([]string{v.value, v.srcFormat, v.dstFormat})
		_ = err
		//t.Logf("INFO: case:%+v, result:%v, err:%v", v, r, err)
		if v.expect != r.String() {
			t.Fatalf("expect:%v, get:%v", v.expect, r.String())
		}
	}
}
*/
