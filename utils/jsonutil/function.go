package jsonutil

import (
	"bytes"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// functions is a map from function names to function.
var functions = map[string]Function{
	// function for number
	"tonumber": funcToNumber,
	"int":      funcInt,
	"max":      funcMax,
	"min":      funcMin,
	"add":      funcAdd,
	"subtract": funcSubtract,
	"multiply": funcMultiply,
	"divide":   funcDivide,
	"mod":      funcMod,
	"sum":      funcSum,
	"average":  funcAverage,

	// function for string
	"tostring":        funcToString,
	"length":          funcLength,
	"runelength":      funcRuneLength,
	"toupper":         funcToUpper,
	"tolower":         funcToLower,
	"substr":          funcSubStr,
	"hasprefix":       funcHasPrefix,
	"hassuffix":       funcHasSuffix,
	"contains":        funcContains,
	"append":          funcAppend,
	"regexmatch":      funcRegexMatch,
	"regexfindall":    funcRegexFindAll,
	"regexreplaceall": funcRegexReplaceAll,
	"formatstring":    funcFormatString,
	"printstring":     funcPrintString,
	"formattime":      funcFormatTime,
	"formatlocaltime": funcFormatLocalTime,
	"replace":         funcReplace,

	// function for string or json
	"decodejson": decodeJSON,

	// function for http
	"httpget":  funcHTTPGet,
	"httppost": funcHTTPPost,
}

// ErrArgsSize return when args for function is wrong.
var ErrArgsSize = errors.New("error args size")

// ErrArgsType return when args type is wrong.
var ErrArgsType = errors.New("error args type")

// Function define a func template.
type Function func(input *JSON, args []Value) (Value, error)

// RegisterFunction register a function to package.
func RegisterFunction(name string, f Function, rewriteIfNameRegistered ...bool) error {
	if f == nil {
		return errors.New("function should not be nil")
	}

	if name == "" {
		return errors.New("function name should not be empty")
	}

	if name[0] >= '0' && name[0] <= '9' {
		return errors.New("function name should not start with number")
	}

	for _, v := range []byte(name) {
		if !isValidCharForFunc(v) {
			return fmt.Errorf("illegal character in function name: '%c'", v)
		}
	}

	name = strings.ToLower(name)
	if len(rewriteIfNameRegistered) == 0 || rewriteIfNameRegistered[0] == false {
		if _, ok := functions[name]; ok {
			return fmt.Errorf("function '%v' already exist", name)
		}
	}

	functions[name] = f
	return nil
}

func isValidCharForFunc(b byte) bool {
	if b >= 'a' && b <= 'z' {
		return true
	}
	if b >= 'A' && b <= 'Z' {
		return true
	}
	if b >= '0' && b <= '9' {
		return true
	}
	if b == '_' || b == '-' {
		return true
	}
	return false
}

//--------function for number------------

func funcToNumber(_ *JSON, args []Value) (Value, error) {
	if len(args) != 1 {
		return Nil, ErrArgsSize
	}

	v, err := args[0].Number()
	if err != nil {
		return Nil, err
	}

	return NewNumber(v), nil
}

func funcInt(_ *JSON, args []Value) (Value, error) {
	if len(args) != 1 {
		return Nil, ErrArgsSize
	}

	v, err := args[0].Number()
	if err != nil {
		return Nil, err
	}

	return NewNumber(float64(int64(v))), nil
}

func funcMax(_ *JSON, args []Value) (Value, error) {
	if len(args) == 0 {
		return Nil, ErrArgsSize
	}
	max, err := args[0].Number()
	if err != nil {
		return Nil, err
	}

	for i := 1; i < len(args); i++ {
		f, err := args[i].Number()
		if err != nil {
			return Nil, err
		}

		if max < f {
			max = f
		}
	}
	return NewNumber(max), nil
}

func funcAdd(_ *JSON, args []Value) (Value, error) {
	if len(args) != 2 {
		return Nil, ErrArgsSize
	}

	a, err := args[0].Number()
	if err != nil {
		return Nil, err
	}

	b, err := args[1].Number()
	if err != nil {
		return Nil, err
	}

	return NewNumber(a + b), nil
}

func funcSubtract(_ *JSON, args []Value) (Value, error) {
	if len(args) != 2 {
		return Nil, ErrArgsSize
	}
	a, err := args[0].Number()
	if err != nil {
		return Nil, err
	}

	b, err := args[1].Number()
	if err != nil {
		return Nil, err
	}

	return NewNumber(a - b), nil
}

func funcMultiply(_ *JSON, args []Value) (Value, error) {
	if len(args) != 2 {
		return Nil, ErrArgsSize
	}

	a, err := args[0].Number()
	if err != nil {
		return Nil, err
	}

	b, err := args[1].Number()
	if err != nil {
		return Nil, err
	}

	return NewNumber(a * b), nil
}

func funcDivide(_ *JSON, args []Value) (Value, error) {
	if len(args) != 2 {
		return Nil, ErrArgsSize
	}
	a, err := args[0].Number()
	if err != nil {
		return Nil, err
	}

	b, err := args[1].Number()
	if err != nil {
		return Nil, err
	}

	if b == 0 {
		return Nil, ErrArgsType
	}
	return NewNumber(a / b), nil
}

func funcMod(_ *JSON, args []Value) (Value, error) {
	if len(args) != 2 {
		return Nil, ErrArgsSize
	}
	a, err := args[0].Int()
	if err != nil {
		return Nil, err
	}

	b, err := args[1].Int()
	if err != nil {
		return Nil, err
	}

	if b == 0 {
		return Nil, ErrArgsType
	}
	return NewNumber(float64(a % b)), nil
}

func funcMin(_ *JSON, args []Value) (Value, error) {
	if len(args) == 0 {
		return Nil, ErrArgsSize
	}

	min, err := args[0].Number()
	if err != nil {
		return Nil, err
	}

	for i := 1; i < len(args); i++ {
		f, err := args[i].Number()
		if err != nil {
			return Nil, err
		}

		if min > f {
			min = f
		}
	}

	return NewNumber(min), nil
}

func funcSum(_ *JSON, args []Value) (Value, error) {
	if len(args) == 0 {
		return Nil, ErrArgsSize
	}

	sum := float64(0)
	for _, v := range args {
		f, err := v.Number()
		if err != nil {
			return Nil, err
		}
		sum += f
	}

	return NewNumber(sum), nil
}

func funcAverage(_ *JSON, args []Value) (Value, error) {
	if len(args) == 0 {
		return Nil, ErrArgsSize
	}
	sum := float64(0)
	for _, v := range args {
		f, err := v.Number()
		if err != nil {
			return Nil, err
		}

		sum += f
	}
	return NewNumber(sum / float64(len(args))), nil
}

//--------function for string------------
func funcToString(_ *JSON, args []Value) (Value, error) {
	if len(args) != 1 {
		return Nil, ErrArgsSize
	}

	return NewString(args[0].String()), nil
}

func funcLength(_ *JSON, args []Value) (Value, error) {
	if len(args) != 1 {
		return Nil, ErrArgsSize
	}
	switch args[0].Type() {
	case TypeJSON:
		tmp, ok := args[0].v.(JSON)
		if !ok {
			return Nil, ErrArgsType
		}
		if tmp.data == nil {
			return NewNumber(0), nil
		}
		array, ok := tmp.data.(Array)
		if ok {
			return NewNumber(float64(len(array))), nil
		}
		m, ok := tmp.data.(Map)
		if ok {
			return NewNumber(float64(len(m))), nil
		}
		return Nil, ErrArgsType
	default:
		f := float64(len(args[0].String()))
		return NewNumber(f), nil
	}
}

func funcRuneLength(_ *JSON, args []Value) (Value, error) {
	if len(args) != 1 {
		return Nil, ErrArgsSize
	}

	f := float64(len([]rune(args[0].String())))
	return NewNumber(f), nil
}

func funcToUpper(_ *JSON, args []Value) (Value, error) {
	if len(args) != 1 {
		return Nil, ErrArgsSize
	}
	return NewString(strings.ToUpper(args[0].String())), nil
}

func funcToLower(_ *JSON, args []Value) (Value, error) {
	if len(args) != 1 {
		return Nil, ErrArgsSize
	}
	return NewString(strings.ToLower(args[0].String())), nil
}

func funcSubStr(_ *JSON, args []Value) (Value, error) {
	l := len(args)
	if l != 2 && l != 3 {
		return Nil, ErrArgsSize
	}
	src := args[0].String()
	max := len(src)
	begin, err := strconv.Atoi(args[1].String())
	if err != nil {
		return Nil, ErrArgsType
	}
	if begin < 0 {
		begin = 0
	}
	end := max
	if l == 3 {
		end, err = strconv.Atoi(args[2].String())
		if err != nil {
			return Nil, ErrArgsType
		}
		if end > max {
			end = max
		}
	}
	if begin >= max {
		return NewString(""), nil
	}
	return NewString(src[begin:end]), nil
}

func funcHasPrefix(_ *JSON, args []Value) (Value, error) {
	if len(args) != 2 {
		return Nil, ErrArgsSize
	}
	return NewBool(strings.HasPrefix(args[0].String(), args[1].String())), nil
}

func funcHasSuffix(_ *JSON, args []Value) (Value, error) {
	if len(args) != 2 {
		return Nil, ErrArgsSize
	}
	return NewBool(strings.HasSuffix(args[0].String(), args[1].String())), nil
}

func funcContains(_ *JSON, args []Value) (Value, error) {
	if len(args) != 2 {
		return Nil, ErrArgsSize
	}
	return NewBool(strings.Contains(args[0].String(), args[1].String())), nil
}

func funcAppend(_ *JSON, args []Value) (Value, error) {
	if len(args) < 2 {
		return Nil, ErrArgsSize
	}
	buf := bytes.Buffer{}
	for _, v := range args {
		buf.WriteString(v.String())
	}
	return NewString(buf.String()), nil
}

func funcRegexMatch(_ *JSON, args []Value) (Value, error) {
	if len(args) != 2 {
		return Nil, ErrArgsSize
	}
	match, err := regexp.MatchString(args[0].String(), args[1].String())
	if err != nil {
		return Nil, err
	}
	return NewBool(match), nil
}

// funcRegexFind return an array of string found.
// args: regexp, src
func funcRegexFindAll(_ *JSON, args []Value) (Value, error) {
	if len(args) != 2 {
		return Nil, ErrArgsSize
	}

	r, err := regexp.Compile(args[0].String())
	if err != nil {
		return Nil, err
	}

	all := r.FindAllString(args[1].String(), 1024)
	if all == nil {
		all = []string{}
	}
	return NewValue(all)
}

// args: regexp, src, replaceTo
func funcRegexReplaceAll(_ *JSON, args []Value) (Value, error) {
	if len(args) != 3 {
		return Nil, ErrArgsSize
	}

	r, err := regexp.Compile(args[0].String())
	if err != nil {
		return Nil, err
	}

	dst := r.ReplaceAllLiteralString(args[1].String(), args[2].String())
	return NewString(dst), nil
}

func funcFormatString(_ *JSON, args []Value) (Value, error) {
	if len(args) < 1 {
		return Nil, ErrArgsSize
	}
	var a []interface{}
	for i := 1; i < len(args); i++ {
		a = append(a, args[i].v)
	}
	s := fmt.Sprintf(args[0].String(), a...)
	return NewString(s), nil
}

func funcPrintString(_ *JSON, args []Value) (Value, error) {
	if len(args) < 1 {
		fmt.Println("ERR_ARGS_SIZE")
		return Nil, ErrArgsSize
	}
	var a []interface{}
	for i := 1; i < len(args); i++ {
		a = append(a, args[i].v)
	}
	s := fmt.Sprintf(args[0].String(), a...)
	fmt.Println(s)
	return NewString(s), nil
}

// funcFormatTime support unix-s,unix-ms,unix-us,unix-ns and other golang time formats.
// args: timeString, srcFormat, dstFormat.
func funcFormatTime(_ *JSON, args []Value) (Value, error) {
	if len(args) != 3 {
		return Nil, ErrArgsSize
	}
	t, err := parseTime(args[0].String(), args[1].String(), false)
	if err != nil {
		return Nil, err
	}
	return NewString(formatTime(t, args[2].String())), nil
}

// parse timeString as local time.
func funcFormatLocalTime(_ *JSON, args []Value) (Value, error) {
	if len(args) != 3 {
		return Nil, ErrArgsSize
	}
	t, err := parseTime(args[0].String(), args[1].String(), true)
	if err != nil {
		return Nil, err
	}
	return NewString(formatTime(t, args[2].String())), nil
}

func parseTime(timeStr string, format string, local bool) (time.Time, error) {
	if strings.ToLower(timeStr) == "now" {
		return time.Now(), nil
	}
	if strings.HasPrefix(format, "unix-") {
		i, err := strconv.ParseInt(timeStr, 10, 64)
		if err != nil {
			return time.Time{}, err
		}
		switch format {
		case "unix-s":
			return time.Unix(i, 0), nil
		case "unix-ms":
			return time.Unix(0, i*1e6), nil
		case "unix-us":
			return time.Unix(0, i*1e3), nil
		case "unix-ns":
			return time.Unix(0, i), nil
		default:
			return time.Time{}, errors.New("invalid format:" + format)
		}
	}

	if local {
		return time.ParseInLocation(format, timeStr, time.Local)
	}
	return time.Parse(format, timeStr)
}

func formatTime(t time.Time, format string) string {
	switch format {
	case "unix-s":
		return strconv.FormatInt(t.Unix(), 10)
	case "unix-ms":
		return strconv.FormatInt(t.UnixNano()/1e6, 10)
	case "unix-us":
		return strconv.FormatInt(t.UnixNano()/1e3, 10)
	case "unix-ns":
		return strconv.FormatInt(t.UnixNano(), 10)
	case "":
		return t.Format(time.RFC3339)
	default:
		return t.Format(format)
	}
}

func funcHTTPGet(_ *JSON, args []Value) (Value, error) {
	if len(args) != 1 {
		return Nil, ErrArgsSize
	}
	if args[0].String() == "" {
		return Nil, errors.New("url is empty")
	}
	u := args[0].String()
	resp, err := http.Get(u)
	if err != nil {
		return Nil, err
	}
	return parseHTTPResponse(resp)
}

// args: url, contentType, body
func funcHTTPPost(_ *JSON, args []Value) (Value, error) {
	if len(args) != 3 {
		return Nil, ErrArgsSize
	}
	if args[0].String() == "" {
		return Nil, errors.New("url is empty")
	}

	resp, err := http.Post(args[0].String(), args[1].String(), strings.NewReader(args[2].String()))
	if err != nil {
		return Nil, err
	}
	return parseHTTPResponse(resp)
}

func parseHTTPResponse(resp *http.Response) (Value, error) {
	if resp == nil {
		return Nil, errors.New("http return nil")
	}

	defer resp.Body.Close()
	j := New()
	j.Set("status", resp.Status)
	j.Set("statusCode", resp.StatusCode)

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return Nil, err
	}

	s := string(body)
	j.Set("rawBody", s)

	bj, err := NewJSON(body)
	if err == nil {
		j.Set("body", bj)
	} else {
		uv, err := url.ParseQuery(s)
		if err == nil && len(uv) > 0 {
			for k, v := range uv {
				if k != "" {
					j.Set("body"+string(delimiter)+k, v[0])
				}
			}
		}
	}

	for k, v := range resp.Header {
		j.Set("header"+string(delimiter)+k, v[0])
	}

	return NewJSONValue(*j), nil
}

func decodeJSON(_ *JSON, args []Value) (Value, error) {
	if len(args) != 1 {
		return Nil, ErrArgsSize
	}
	v := args[0]
	switch v.Type() {
	case TypeJSON:
		j, _ := v.JSON()
		return NewJSONValue(j), nil

	case TypeString:
		j, err := NewJSON([]byte(v.String()))
		if err != nil {
			return Nil, err
		}
		return NewJSONValue(*j), nil

	default:
		return Nil, ErrArgsType
	}
}

func funcReplace(_ *JSON, args []Value) (Value, error) {
	if len(args) != 3 {
		return Nil, ErrArgsSize
	}
	src := args[0].String()
	old := args[1].String()
	new := args[2].String()

	dst := strings.Replace(src, old, new, -1)
	return NewString(dst), nil
}
