package webutil

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
)

const (
	CtxFieldResp = "rawRespJson"
)

type CommResp struct {
	Ret     int64  `json:"ret"`
	Message string `json:"message"`
}

func ParseReq(c *gin.Context, req interface{}) error {
	rawData, err := c.GetRawData()
	if err != nil {
		return err
	}

	err = json.Unmarshal(rawData, req)
	if err != nil {
		return err
	}

	return nil
}

func Return(c *gin.Context, obj interface{}) {

	respJson, _ := json.Marshal(obj)
	c.Set(CtxFieldResp, string(respJson))
	c.JSON(200, obj)
}

func ReturnOK(c *gin.Context) {
	Return(c, getCommRespOk())
}

func ReturnErr(c *gin.Context, err error) {
	Return(c, &CommResp{
		Ret:     -1,
		Message: err.Error(),
	})
}

func getCommRespOk() *CommResp {
	return &CommResp{
		Ret:     0,
		Message: "ok",
	}
}
