package webutil

import (
	"bytes"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"logxxx.com/test/v2/utils/log"
	"time"
)

const (
	ShowBodyMaxLen = 512
)

func PrintURL(c *gin.Context) {
	st := time.Now()
	c.Next()
	d := time.Since(st)
	log.Infof("[Req]url:%v st=%.2fs", c.Request.URL.String(), d.Seconds())
}

func PrintReq(c *gin.Context) {
	st := time.Now()

	rawReq, _ := c.GetRawData()

	c.Request.Body = ioutil.NopCloser(bytes.NewReader(rawReq))

	c.Next()

	showResp := ""
	respObj, ok := c.Get(CtxFieldResp)
	if ok {
		showResp = fmt.Sprintf("%v", respObj)
		if len(showResp) > ShowBodyMaxLen {
			showResp = showResp[:ShowBodyMaxLen] + fmt.Sprintf("...(%v more words ignored)", len(showResp)-ShowBodyMaxLen)
		}
	}

	d := time.Since(st)

	cuttedReq := ""
	if len(rawReq) > ShowBodyMaxLen {
		cuttedReq = string(rawReq[:ShowBodyMaxLen]) + fmt.Sprintf("...(%v more words ignored)", len(rawReq)-ShowBodyMaxLen)
	} else {
		cuttedReq = string(rawReq)
	}

	log.Infof("[Req]url:%v st=%.2fs rawReq:%v rawResp:%v",
		c.Request.URL.String(), d.Seconds(), string(cuttedReq), showResp)
}
