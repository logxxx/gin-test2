package log

import "log"

func Infof(format string, v ...interface{}) {
	log.Printf(format, v...)
}

func Errorf(format string, v ...interface{}) {
	log.Printf(format, v)
}
