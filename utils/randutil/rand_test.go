package randutil_test

import (
	"fmt"
	"logxxx.com/test/v2/utils/randutil"
	"testing"
)

func TestRand(t *testing.T) {
	for i := 0; i < 10; i++ {
		fmt.Println(randutil.RandString(10))
	}
}

func TestRandSinogram(t *testing.T) {
	resp1 := randutil.RandSinogram(4)
	t.Logf("resp1:%v", resp1)
}
