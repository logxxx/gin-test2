package randutil

import (
	"math/rand"
	"time"
)

var (
	r *rand.Rand
)

func init() {
	r = rand.New(rand.NewSource(time.Now().Unix()))
}

func RandString(length int) string {
	bytes := make([]byte, length)
	for i := 0; i < length; i++ {
		b := r.Intn(26) + 65
		bytes[i] = byte(b)
	}
	return string(bytes)
}

func RandSinogram(length int) string {
	a := make([]rune, length)
	for i := range a {
		a[i] = rune(RandInt(20000, 40000))
	}
	return string(a)
}

func RandInt(min, max int64) int64 {
	return min + r.Int63n(max-min)
}

func RandIntn(max int64) int64 {
	return RandInt(0, max)
}
