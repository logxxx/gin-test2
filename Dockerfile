FROM golang:latest

COPY . .
ENV GOPATH=''
ENV GO111MODULE=on
RUN go env -w GOPROXY=https://goproxy.cn
RUN go mod tidy
RUN go build cmd/main.go

EXPOSE 8080
ENTRYPOINT ["./main"]