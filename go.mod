module logxxx.com/test/v2

go 1.14

require (
	github.com/garyburd/redigo v1.6.2 // indirect
	github.com/gin-gonic/gin v1.7.4
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/onsi/gomega v1.16.0 // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/stretchr/testify v1.7.0 // indirect
	github.com/ugorji/go v1.2.6 // indirect
	golang.org/x/net v0.0.0-20210913180222-943fd674d43e // indirect
	golang.org/x/sys v0.0.0-20210910150752-751e447fb3d0 // indirect
	gopkg.in/bsm/ratelimit.v1 v1.0.0-20160220154919-db14e161995a // indirect
	gopkg.in/redis.v3 v3.6.4
	gopkg.in/yaml.v2 v2.4.0
)
